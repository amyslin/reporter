@extends('layouts.site')

@section('title', $page->title)

@section('content')

{!! $page->content !!}

{!! Request::path() !!}

@endsection