
<h4 class="header blue bolder smaller">@yield('title')</h4>
{{ Form::model($model, $config['formOptions']) }}

@if(isset($config['tabs']) && count($config['tabs']))
<ul class="nav nav-tabs padding-18">
       	@foreach ($config['tabs'] as $key => $tab)   
       	<li {{ ($loop->first) ? 'class=active' : '' }} >
       		<a href="#{{ $key }}" data-toggle="tab"><i class="{{ $tab['class'] }}"></i>{{ $tab['name'] }}</a>
       	</li>
       	@endforeach
</ul>
<div class="tab-content no-border padding-24">
@foreach ($config['tabs'] as $key => $tab)
	<div id="{{ $key }}" class="tab-pane {{ ($loop->first) ? 'active' : '' }}">
		@if(isset($tab['content']) && !empty($tab['content']))
			@include($tab['content'], compact('fields'))
		@else
			@foreach($model->getTableColumns(['id', 'created_at', 'updated_at']) as $attribute)
				@if(isset($config['fieldsOptions'][$attribute]) && isset($config['fieldsOptions'][$attribute]['type']))		
			 		@include('widgets.model_form.' . $config['fieldsOptions'][$attribute]['type'] , ['errors' => $errors, 'model'=>$model, 'attribute' => $attribute])
			 	@else
			 		@include('widgets.model_form.text' , ['errors' => $errors, 'model'=>$model, 'attribute' => $attribute])
			 	@endif
			@endforeach
		@endif
	</div>
@endforeach
</div>
@else
	@foreach($model->getTableColumns(['id', 'created_at', 'updated_at']) as $attribute)
		@if(isset($config['fieldsOptions'][$attribute]) && isset($config['fieldsOptions'][$attribute]['type']))		
	 		@include('widgets.model_form.' . $config['fieldsOptions'][$attribute]['type'] , ['errors' => $errors, 'model'=>$model, 'attribute' => $attribute])
	 	@else
	 		@include('widgets.model_form.text' , ['errors' => $errors, 'model'=>$model, 'attribute' => $attribute])
	 	@endif
	@endforeach
@endif
	<div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-info">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Сохранить
                </a>

                &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Очистить
                </button>
            </div>
    </div>
{{ Form::close() }}