<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-about">{{ $model->getAttributeLabel($attribute) }}</label>
    <div class="col-sm-9">
    	<div id="{{ $model->getAttributeLabel($attribute) }}" class="wysiwyg-editor"></div>
    	{!! Form::hidden($attribute, null, array('id' => $model->getAttributeLabel($attribute) . '-value')) !!}
    </div>
</div>