{{ Form::model($model, $config['formOptions']) }}


@foreach ($model->fields()->get() as $field) 
	@if(!empty($field->data_type))
		@include('widgets.dynamic_form.' . $field->data_type , ['errors' => $errors, 'field'=>$field])
	@else
		@include('widgets.dynamic_form.text' , ['errors' => $errors, 'field'=>$field])
	@endif
@endforeach

	<div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-info">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Сохранить
                </a>

                &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Очистить
                </button>
            </div>
    </div>
{{ Form::close() }}