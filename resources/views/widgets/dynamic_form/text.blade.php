<div class="form-group {{ (($errors->has('title'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">{{ $field->name }}</label>
    <div class="col-sm-3">
    	{!! Form::text('field_' . $field->id , null, array('placeholder' => $field->name, 'class' => 'col-xs-12' )) !!}
    	{{--
    	@foreach($errors->get($attribute) as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
	    --}}
    </div>
</div>
