<div class="form-group {{ (($errors->has('field_' . $field->id))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">{{ $field->name }}</label>
    <div class="col-sm-3">
    	<div class="input-group">
        	{!! Form::text('field_' . $field->id, null, array('id' => 'id-date-picker-2', 'placeholder' => $field->name, 'class' => 'form-control date-picker')) !!}
            <span class="input-group-addon">
                <i class="fa fa-calendar bigger-110"></i>
            </span>
        </div>
        {{--
	    @foreach($errors->get($attribute) as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
	    --}}
    </div>
</div>