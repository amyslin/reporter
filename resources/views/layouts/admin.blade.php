<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>@yield('title')</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&subset=cyrillic-ext" rel="stylesheet">
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}" />
		<link rel="stylesheet" href="{{ asset('/css/font-awesome.css') }}" />

		<!-- page specific plugin styles -->
        <link rel="stylesheet" href="{{ asset('/css/fullcalendar.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/bootstrap-datepicker3.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/daterangepicker.css') }}" />
        <link rel="stylesheet" href="{{ asset('/css/bootstrap-datetimepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/chosen.css') }}" />
        
        <link rel="stylesheet" href="{{ asset('/css/ui.jqgrid.css') }}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ asset('/css/ace.css?1') }}" class="ace-main-stylesheet" id="main-ace-style" />
		

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
        <link rel="stylesheet" href="{{ asset('/css/my.css') }}" />

		<!-- ace settings handler -->
		<script src="{{ asset('/js/ace-extra.js') }}"></script>
		<script src="{{ asset('/js/jquery.js') }}"></script>
		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="/js/html5shiv.js"></script>
		<script src="/js/respond.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		@include('admin.navbar')
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			@include('admin.sidebar')
			<div class="main-content">
				<div class="main-content-inner">
					@yield('content')
				</div>
			</div>
			<div class="footer">
			    <div class="footer-inner">
			        <!-- #section:basics/footer -->
			        <div class="footer-content">
			            <span class="bigger-120">
			                <span class="blue bolder">TopDigital</span>
			                &copy; 2017
			            </span>
			        </div>
			        <!-- /section:basics/footer -->
			    </div>
			</div>
			
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
			    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div>	
	<!-- basic scripts -->
	
	<!--[if !IE]> -->
	
	
	<!-- <![endif]-->
	
	<!--[if IE]>
	<script src="/js/jquery1x.js"></script>
	<![endif]-->
	<script type="text/javascript">
	    if('ontouchstart' in document.documentElement) document.write("<script src='/js/jquery.mobile.custom.js'>"+"<"+"/script>");
	</script>
	<script src="{{ asset('/js/bootstrap.js') }}"></script>
	
	<!-- page specific plugin scripts -->
	
	<!--[if lte IE 8]>
	  <script src="/js/excanvas.js"></script>
	<![endif]-->
	<script src="{{ asset('/js/jquery-ui.custom.js') }}"></script>
	<script src="{{ asset('/js/jquery.ui.touch-punch.js') }}"></script>
	<script src="{{ asset('/js/jquery.easypiechart.js') }}"></script>
	<script src="{{ asset('/js/jquery.sparkline.js') }}"></script>
	<script src="{{ asset('/js/chosen.jquery.js') }}"></script>
	<script src="{{ asset('/js/flot/jquery.flot.js') }}"></script>
	<script src="{{ asset('/js/flot/jquery.flot.pie.js') }}"></script>
	<script src="{{ asset('/js/flot/jquery.flot.resize.js') }}"></script>
	
	
	<script src="{{ asset('/js/date-time/moment.js') }}"></script>
	<script src="{{ asset('/js/date-time/bootstrap-datepicker.js') }}"></script>
	<script src="{{ asset('/js/date-time/bootstrap-timepicker.js') }}"></script>
	<script src="{{ asset('/js/date-time/daterangepicker.js') }}"></script>
	<script src="{{ asset('/js/bootstrap-multiselect.js') }}"></script>
	<script src="{{ asset('/js/date-time/bootstrap-datetimepicker.js') }}"></script>
	<script src="{{ asset('/js/bootstrap-wysiwyg.js') }}"></script>
	
	<!-- ace scripts -->
	<script src="{{ asset('/js/ace/tree.min.js') }}" ></script>
	<script src="{{ asset('/js/ace/elements.scroller.js') }}"></script>
	<script src="{{ asset('/js/ace/elements.colorpicker.js') }}"></script>
	<script src="{{ asset('/js/ace/elements.fileinput.js') }}"></script>
	<script src="{{ asset('/js/ace/elements.typeahead.js') }}"></script>
	<script src="{{ asset('/js/ace/elements.wysiwyg.js') }}"></script>
	<script src="{{ asset('/js/ace/elements.spinner.js') }}"></script>
	<script src="{{ asset('/js/ace/elements.treeview.js') }}"></script>
	<script src="{{ asset('/js/ace/elements.wizard.js') }}"></script>
	<script src="{{ asset('/js/ace/elements.aside.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.ajax-content.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.touch-drag.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.sidebar.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.sidebar-scroll-1.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.submenu-hover.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.widget-box.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.settings.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.settings-rtl.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.settings-skin.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.widget-on-reload.js') }}"></script>
	<script src="{{ asset('/js/ace/ace.searchbox-autocomplete.js') }}"></script>
	<script src="{{ asset('/js/jqGrid/jquery.jqGrid.js') }}"></script>
	<script src="{{ asset('/js/jqGrid/i18n/grid.locale-ru.js') }}"></script>
	<script src="{{ asset('/js/sagrado.js') }}"></script>
	
	</body>
</html>