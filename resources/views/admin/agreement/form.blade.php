<div class="page-content">
	{{ Form::model($model, ['class'=> 'form-horizontal', 'id'=>'form-editor' , 'url' => 'admin/agreement', 'enctype' => 'multipart/form-data']) }}    
        

        

<h4 class="header blue bolder smaller">Лицензионное соглашение</h4>

<div class="form-group {{ (($errors->has('title'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Название</label>
    <div class="col-sm-3">
    	{!! Form::text('title', null, array('placeholder' => 'Название', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-about">Описание</label>
    <div class="col-sm-9">
    	<div id="editor" class="wysiwyg-editor">{!! $model['content'] !!}</div>
    	{!! Form::hidden('content', null, array('id' => 'editor-value')) !!}
    </div>
</div>
<div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-info">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Сохранить
                </a>

                &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Очистить
                </button>
            </div>
        </div>
    {{ Form::close() }}
</div>