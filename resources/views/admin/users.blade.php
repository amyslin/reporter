@extends('layouts.admin')

@section('title', 'Index')

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
        
        	<a href="{{ url('admin/users/create') }}" class="btn btn-xs btn-success">
                <i class="ace-icon fa fa-plus bigger-120"></i> Добавить пользователя
         	</a>
         	<div class="space-6"></div>
         	<table class="table  table-bordered table-hover">
         		<thead>
         			<th>
         				
         			</th>
         			<th>
         				Имя пользователя
         			</th>
         			<th>
         				Email
         			</th>
         			<th>
         				Сервисы (профили)
         			</th>
         			<th>
         				API token
         			</th>
         			<th>
         				Роли
         			</th>
         			<th>
         				Дата создания
         			</th>
         			<th>
         				Дата модификации
         			</th>
         			<th>
         			
         			</th>
         		</thead>
         		<tbody>
         			@foreach ($users as $user)
         			<tr>
         				<td>
         					<label class="pos-rel">
         						<input type="checkbox" class="ace" />
         						<span class="lbl"></span>
         					</label>
         				</td>
         				<td>
         					{{ $user->username }}
         				</td>
         				<td>
         					{{ $user->email }}
         				</td>
         				<td>
         					@foreach ($user->profiles as $profile)
         						<span class="label">{{ $profile->service }}</span>
         					@endforeach
         				</td>
         				<td>
         					@if (!empty($token = $user->token()))
         						{{ $token->id }}
         					@endif
         				</td>
         				<td>
         					@foreach ($user->roles as $role)
         						<span class="label">{{ $role->slug }}</span>
         					@endforeach
         				</td>
         				<td>
         					{{ $user->created_at }}
         				</td>
         				<td>
         					{{ $user->updated_at }}
         				</td>
         				<td>
         					<div class="hidden-sm hidden-xs action-buttons">
         						<a href="{{ URL::to('admin/users/view/' . $user->id) }}" class="blue">
         							<i class="ace-icon fa fa-search-plus bigger-130"></i>
         						</a>
         						<a href="{{ URL::to('admin/users/edit/' . $user->id) }}" class="green">
         							<i class="ace-icon fa fa-pencil bigger-130"></i>
         						</a>
         						<a href="#" class="red">
         							<i class="ace-icon fa fa-trash-o bigger-130"></i>
         						</a>
         					</div>
         				</td>
         			</tr>
         			@endforeach
         		</tbody>
         	</table>
         	{{ $users->links() }}
        	<div>
        		<ul class="ace-thumbnails clearfix">
        		<!--
        			{{-- 
        			@foreach ($clubs as $club)
        				<li>
        					<a href="{{ URL::to('admin/main/edit/' . $club->id) }}" data-rel="colorbox">
        						<span style="display: block; width: 180px; height: 255px; background: url({{ (!empty($image = $club->images->first())) ? $image->source : url('img/noimage.png') }}) 50% 0% no-repeat; background-size: cover;"> </span>
                        	</a>
                        	<div class="tags">
	                            <span class="label-holder">
	                                <span class="label label-info">{{ $club->title }}</span>
	                            </span>
                        	</div>
                        	<div class="tools tools-top in">
                            	<a href="{{ URL::to('admin/main/edit/' . $club->id) }}">
                                	<i class="ace-icon fa fa-pencil"></i>
                            	</a>
                            	<a href="{{ URL::to('admin/main/delete/' . $club->id) }}" class="delete-object">
                                	<i class="ace-icon fa fa-trash-o"></i>
                            	</a>
                        	</div>
        				</li>
        			@endforeach
        			--}}
        		 -->
        		</ul>
        	</div>
		</div>
	</div>
</div>
@endsection