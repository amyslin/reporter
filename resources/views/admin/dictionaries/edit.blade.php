@extends('layouts.admin')

@section('title', 'Справочники')

@section('content')
	<div class="page-content">
		@widget('ModelForm', ['fieldsOptions' => [
			'name' => ['type' => 'text'],
			'comment' => ['type' => 'html'],
		],
		'order' => [
			
		],
		'groups' => [
			
		],
		'tabs' => [
			'form' => [
				'name' => 'Справочник',
				'fields' => ['name'],
				'class' => 'green ace-icon fa fa-pencil-square-o bigger-120',
			],
			'fields' => [
				'name' => 'Значения справочника',
				'content' => 'admin.dictionaries.values',
				'class' => 'blue ace-icon fa fa-list bigger-120'
			]
		],
		], $model, $errors)
	</div>
@endsection