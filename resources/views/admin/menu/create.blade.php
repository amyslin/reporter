@extends('layouts.admin')

@section('title', 'Добавить страницу')

@section('content')

	@include('admin.menu.create-form')
	
@endsection