<div class="page-content">
	{{ Form::model($model, ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}    
        

        

<h4 class="header blue bolder smaller">Редактирование меню</h4>

<div class="form-group {{ (($errors->has('title'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Название</label>
    <div class="col-sm-3">
    	{!! Form::text('title', null, array('placeholder' => 'Название', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>

<div class="form-group {{ (($errors->has('place'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Расположение</label>
    <div class="col-sm-3">
    	{!! Form::text('place', null, array('placeholder' => 'Расположение', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('place') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>

<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Родительский пункт</label>
						<div class="col-sm-8">
							<select name="parent_id">
								<option value="" >..</option>
								@foreach ($parents as $parent)
									<option value="{{ $parent->id }}" {{ ($parent->id == $model->parent_id) ? 'selected' : '' }} >{{ $parent->title }}</option>	
								@endforeach								
							</select>
							@foreach($errors->get('parent_id') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
</div>

<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Тип</label>
						<div class="col-sm-8">
							<select name="type">
								<option value="link" {{ ($model->type == 'link') ? 'selected' : '' }}>Ссылка</option>	
								<option value="page" {{ ($model->type == 'page') ? 'selected' : '' }}>Страница</option>
								<option value="view" {{ ($model->type == 'view') ? 'selected' : '' }}>View</option>
								<option value="controller" {{ ($model->type == 'controller') ? 'selected' : '' }}>Контроллер</option>
															
							</select>
							@foreach($errors->get('type') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
</div>
<div class="form-group {{ (($errors->has('link'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Ссылка</label>
    <div class="col-sm-3">
    	{!! Form::text('link', null, array('placeholder' => 'Ссылка', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('link') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Страница</label>
						<div class="col-sm-8">
							<select name="page_id">
								<option value="">..</option>
								@foreach($pages as $page)
								<option value="{{ $page->id }}" {{ ($page->id == $model->page_id) ? 'selected' : '' }}>{{ $page->title }}</option>
								
								@endforeach
															
							</select>
							@foreach($errors->get('type') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
</div>
{{--
<div class="form-group {{ (($errors->has('active'))? 'has-error' : '') }}">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-username">Активность</label>
	<label class="middle">
		{!! Form::checkbox('active', null, ['class' => 'ace', 'value' => 1]) !!}
	</label>
</div>
--}}

<div class="form-group {{ (($errors->has('view'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Путь к файлу шаблона</label>
    <div class="col-sm-3">
    	{!! Form::text('view', null, array('placeholder' => 'Путь к файлу шаблона', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
<div class="form-group {{ (($errors->has('controller'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Контроллер</label>
    <div class="col-sm-3">
    	{!! Form::text('controller', null, array('placeholder' => 'Контроллер', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>

<div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-info">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Сохранить
                </a>

                &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Очистить
                </button>
            </div>
        </div>
    {{ Form::close() }}
</div>