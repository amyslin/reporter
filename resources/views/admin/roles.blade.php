@extends('layouts.admin')

@section('title', 'Index')

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
        
        	<a href="{{ url('admin/roles/create/role') }}" class="btn btn-xs btn-success">
                <i class="ace-icon fa fa-plus bigger-120"></i> Добавить роль
         	</a>
         	
         	<a href="{{ url('admin/roles/create/permission') }}" class="btn btn-xs btn-success">
                <i class="ace-icon fa fa-plus bigger-120"></i> Добавить привилегию
         	</a>
         	
         	<div class="space-6"></div>
         	<div class="tabbable">
         	<ul class="nav nav-tabs padding-18">
	         	<li class="active">
	         		<a href="#roles" data-toggle="tab"><i class="blue ace-icon fa fa-users bigger-120"></i>Роли</a>
	         	</li>
	         	<li>
	         		<a href="#permission" data-toggle="tab"><i class="green ace-icon fa fa-users bigger-120"></i>Привилегии</a>
	         	</li>
         	</ul>
         	<div class="tab-content no-border padding-24">
         	<div id="roles" class="tab-pane active">
	         	<table class="table  table-bordered table-hover">
	         		<thead>
	         			<th>
	         				
	         			</th>
	         			<th>
	         				Наименование
	         			</th>
	         			<th>
	         				Slug
	         			</th>
	         			<th>
	         				Описание
	         			</th>
	         			<th>
	         				Уровень
	         			</th>
	         			<th>
	         				Дата создания
	         			</th>
	         			<th>
	         				Дата модификации
	         			</th>
	         			<th>
	         			
	         			</th>
	         		</thead>
	         		<tbody>
	         			@foreach ($roles as $role)
	         			<tr>
	         				<td>
	         					<label class="pos-rel">
	         						<input type="checkbox" class="ace" />
	         						<span class="lbl"></span>
	         					</label>
	         				</td>
	         				<td>
	         					{{ $role->name }}
	         				</td>
	         				<td>
	         					{{ $role->slug }}
	         				</td>
	         				<td>
	         					{{ $role->description }}
	         				</td>
	         				<td>
	         					{{ $role->level }}
	         				</td>
	         				
	         				<td>
	         					{{ $role->created_at }}
	         				</td>
	         				<td>
	         					{{ $role->updated_at }}
	         				</td>
	         				<td>
	         					<div class="hidden-sm hidden-xs action-buttons">
	         						<a href="{{ URL::to('admin/roles/edit/role.' . $role->id) }}" class="green">
	         							<i class="ace-icon fa fa-pencil bigger-130"></i>
	         						</a>
	         						<a href="{{ URL::to('admin/roles/delete/role.' . $role->id) }}" class="red">
	         							<i class="ace-icon fa fa-trash-o bigger-130"></i>
	         						</a>
	         					</div>
	         				</td>
	         			</tr>
	         			@endforeach
	         		</tbody>
	         	</table>
         	</div>
         	
         	<div id="permission" class="tab-pane">
         		<table class="table  table-bordered table-hover">
	         		<thead>
	         			<th>
	         				
	         			</th>
	         			<th>
	         				Наименование
	         			</th>
	         			<th>
	         				Slug
	         			</th>
	         			<th>
	         				Описание
	         			</th>
	         			<th>
	         				Модель
	         			</th>
	         			<th>
	         				Дата создания
	         			</th>
	         			<th>
	         				Дата модификации
	         			</th>
	         			<th>
	         			
	         			</th>
	         		</thead>
	         		<tbody>
	         			@foreach ($permissions as $role)
	         			<tr>
	         				<td>
	         					<label class="pos-rel">
	         						<input type="checkbox" class="ace" />
	         						<span class="lbl"></span>
	         					</label>
	         				</td>
	         				<td>
	         					{{ $role->name }}
	         				</td>
	         				<td>
	         					{{ $role->slug }}
	         				</td>
	         				<td>
	         					{{ $role->description }}
	         				</td>
	         				<td>
	         					{{ $role->model }}
	         				</td>
	         				
	         				<td>
	         					{{ $role->created_at }}
	         				</td>
	         				<td>
	         					{{ $role->updated_at }}
	         				</td>
	         				<td>
	         					<div class="hidden-sm hidden-xs action-buttons">
	         						<a href="{{ URL::to('admin/roles/edit/permission.' . $role->id) }}" class="green">
	         							<i class="ace-icon fa fa-pencil bigger-130"></i>
	         						</a>
	         						<a href="{{ URL::to('admin/roles/delete/permission.' . $role->id) }}" class="red">
	         							<i class="ace-icon fa fa-trash-o bigger-130"></i>
	         						</a>
	         					</div>
	         				</td>
	         			</tr>
	         			@endforeach
	         		</tbody>
	         	</table>
         	
         	</div>
         	
         	</div>
         	</div>
        	<div>
        		<ul class="ace-thumbnails clearfix">
        		<!--
        			{{-- 
        			@foreach ($clubs as $club)
        				<li>
        					<a href="{{ URL::to('admin/main/edit/' . $club->id) }}" data-rel="colorbox">
        						<span style="display: block; width: 180px; height: 255px; background: url({{ (!empty($image = $club->images->first())) ? $image->source : url('img/noimage.png') }}) 50% 0% no-repeat; background-size: cover;"> </span>
                        	</a>
                        	<div class="tags">
	                            <span class="label-holder">
	                                <span class="label label-info">{{ $club->title }}</span>
	                            </span>
                        	</div>
                        	<div class="tools tools-top in">
                            	<a href="{{ URL::to('admin/main/edit/' . $club->id) }}">
                                	<i class="ace-icon fa fa-pencil"></i>
                            	</a>
                            	<a href="{{ URL::to('admin/main/delete/' . $club->id) }}" class="delete-object">
                                	<i class="ace-icon fa fa-trash-o"></i>
                            	</a>
                        	</div>
        				</li>
        			@endforeach
        			--}}
        		 -->
        		</ul>
        	</div>
		</div>
	</div>
</div>
@endsection