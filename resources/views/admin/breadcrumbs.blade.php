@if ($breadcrumbs)
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
            @if (!$breadcrumb->last)
                <li>
                	<i class="ace-icon fa fa-home home-icon"></i>
                	<a href="{{{ $breadcrumb->url }}}">{{{ $breadcrumb->title }}}</a>
                </li>
            @else
                <li class="active">{{{ $breadcrumb->title }}}</li>
            @endif
        @endforeach
    </ul>
</div>
@endif