<div class="page-content">
	<div class="col-sm-6">
		<div class="widget-box">
			<div class="widget-header">
				<h4 class="widget-title">Новый пользователь</h4>
			</div>
			<div class="widget-body">
			{{ Form::open(['class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
				<div class="widget-main">
				
					<div class="row">
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Логин (Email)</label>
						<div class="col-sm-8">
							{!! Form::text('email', null, array('id' => 'form-field-username', 'placeholder' => 'Email', 'class' => 'col-xs-12' )) !!}
							@foreach($errors->get('email') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Пароль</label>
						<div class="col-sm-8">
							{!! Form::password('password', null, array('id' => 'form-field-password', 'placeholder' => 'Пароль', 'class' => 'col-xs-12' )) !!}
							@foreach($errors->get('password') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Подтверждение пароля</label>
						<div class="col-sm-8">
							{!! Form::password('password_confirmation', null, array('id' => 'form-field-password_confirmation', 'placeholder' => 'Пароль', 'class' => 'col-xs-12' )) !!}
							@foreach($errors->get('password_confirmation') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Группа</label>
						<div class="col-sm-8">
							<select class="multiselect" multiple="" name="roles">
								@foreach ($groups as $group)
									<option value="{{ $group->id }}">{{ $group->slug }}</option>
								@endforeach
							</select>
							@foreach($errors->get('roles') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
					</div>
					</div>
					   
				
				</div>
				<div class="widget-main no-padding">
					<div class="form-actions center">
						<button type="submit" class="btn btn-sm btn-success">Создать</button>
						<button type="reset" class="btn btn-sm">Очистить</button>
					</div>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>