@extends('layouts.admin')

@section('title', 'Редактирование пользователя')

@section('content')

<div class="page-content">
	<div class="page-header">
		<h1>Пользователь</h1>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-3 center">
			<span class="profile-picture">
				<img class="editable img-responsive" src="{{ asset('/avatars/profile-pic.jpg') }}"/>
			</span>
			<div class="space space-4"></div>
			<a href="{{ URL::to('admin/users/edit/' . $model->id ) }}" class="btn btn-sm btn-block btn-success">Редактировать</a>
			<a href="{{ URL::to('admin/users/delete/' . $model->id ) }}" class="btn btn-sm btn-block btn-danger">Удалить</a>
		</div>
		<div class="col-xs-12 col-sm-9">
			<h4 class="blue">{{ $model->username }}</h4>
			<div class="profile-user-info">
				<div class="profile-info-row">
					<div class="profile-info-name">Логин</div>
					<div class="profile-info-value">{{ $model->email }}</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name">Группы</div>
					<div class="profile-info-value">
						@foreach($model->roles as $role)
							<span class="label">{{ $role->slug }}</span>
						@endforeach
					</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name">Дата создания</div>
					<div class="profile-info-value">{{ $model->created_at }}</div>
				</div>
				<div class="profile-info-row">
					<div class="profile-info-name">Дата обновления</div>
					<div class="profile-info-value">{{ $model->updated_at }}</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection