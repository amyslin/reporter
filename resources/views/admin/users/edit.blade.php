@extends('layouts.admin')

@section('title', 'Редактирование пользователя')

@section('content')

	@include('admin.users.form')
	
@endsection