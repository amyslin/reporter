@extends('layouts.admin')

@section('title', 'Страницы')

@section('content')

<div class="page-content">
	@widget('DynamicForm', ['fieldOptions' => []], 1, null)
</div>

@endsection