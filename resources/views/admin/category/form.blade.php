<div class="page-content">
	{{ Form::model($model, ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}    
        

        

<h4 class="header blue bolder smaller">Добавить категорию</h4>

<div class="form-group {{ (($errors->has('title'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Название</label>
    <div class="col-sm-3">
    	{!! Form::text('title', null, array('placeholder' => 'Название', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
<div class="form-group {{ (($errors->has('code'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Путь</label>
    <div class="col-sm-3">
    	{!! Form::text('path', null, array('placeholder' => 'Путь', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('path') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>

<div class="form-group {{ (($errors->has('code'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Порядок</label>
    <div class="col-sm-3">
    	{!! Form::text('order', null, array('placeholder' => 'Порядок', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('order') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>

{{--
<div class="form-group {{ (($errors->has('active'))? 'has-error' : '') }}">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-username">Активность</label>
	<label class="middle">
		{!! Form::checkbox('active', null, ['class' => 'ace', 'value' => 1]) !!}
	</label>
</div>
--}}


		<div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-info">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Сохранить
                </a>

                &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Очистить
                </button>
            </div>
        </div>
    {{ Form::close() }}
</div>