@extends('layouts.admin')

@section('title', 'Добавить категорию')

@section('content')

	@include('admin.category.create-form')
	
@endsection