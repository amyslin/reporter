@extends('layouts.admin')

@section('title', 'Добавить собыие')

@section('content')
	<div class="page-content">
		@widget('ModelForm', ['fieldsOptions' => [
			'name' => ['type' => 'text'],
			'comment' => ['type' => 'html'],
		],
		'order' => [
			
		],
		'groups' => [
			
		],
		'tabs' => [
			'form' => [
				'name' => 'Форма',
				'fields' => ['name', 'comment'],
				'class' => 'green ace-icon fa fa-pencil-square-o bigger-120',
			],
			'fields' => [
				'name' => 'Поля',
				'content' => 'admin.forms.fields',
				'class' => 'blue ace-icon fa fa-list bigger-120'
			]
		],
		], $model, $errors)
	</div>
@endsection