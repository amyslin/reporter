@extends('layouts.admin')

@section('title', 'Заполненные формы')

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">     	
         	
         	
         	<div class="space-6"></div>
         	
         	
         
	         	<table class="table  table-bordered table-hover">
	         		<thead>
	         			<th>
	         				
	         			</th>
	         			<th>
	         				IP
	         			</th>
	         			<th>
	         				Пользователь
	         			</th>
	         			<th>
	         				Дата заполнения
	         			</th>
	         			@foreach ($model->fields as $field)
	         				<th>
	         					{{ $field->name }}
	         				</th>
	         			@endforeach
	         			<th>
	         			
	         			</th>
	         		</thead>
	         		<tbody>
	         			@foreach ($model->items()->with(['user', 'values'])->get() as $item)
	         			<tr>
	         				<td>
	         					<label class="pos-rel">
	         						<input type="checkbox" class="ace" />
	         						<span class="lbl"></span>
	         					</label>
	         				</td>
	         				<td>
	         					{{ $item->ip }}
	         				</td>
	         				<td>
	         					{{ (!empty($item->user)) ? $item->user->username : 'Неавторизованный' }}
	         				</td>
	         				
	         				<td>
	         					{{ $item->created_at }}
	         				</td>
	         				<?php $coll = $item->values()->get(); ?>
	         				@foreach ($model->fields as $field)
	         					<?php $val = $coll->where('field_id', $field->id)->first(); ?>
	         					<td>
	         						{{ (!empty($val)) ? $val->value : '' }}
	         					</td>
	         				@endforeach
	         				<td>
	         					{{--
	         					<div class="hidden-sm hidden-xs action-buttons">
	         						<a href="{{ URL::to('admin/form/view/' . $model->id) }}" class="blue">
	         							<i class="ace-icon fa fa-eye bigger-130"></i>
	         						</a>
	         						<a href="{{ URL::to('admin/form/edit/' . $model->id) }}" class="green">
	         							<i class="ace-icon fa fa-pencil bigger-130"></i>
	         						</a>
	         						<a href="{{ URL::to('admin/form/delete/' . $model->id) }}" class="red">
	         							<i class="ace-icon fa fa-trash-o bigger-130"></i>
	         						</a>
	         					</div>
	         					--}}
	         				</td>
	         			</tr>
	         			@endforeach
	         		</tbody>
	         	</table>
        	<div>
        		<ul class="ace-thumbnails clearfix">
        		<!--
        			{{-- 
        			@foreach ($clubs as $club)
        				<li>
        					<a href="{{ URL::to('admin/main/edit/' . $club->id) }}" data-rel="colorbox">
        						<span style="display: block; width: 180px; height: 255px; background: url({{ (!empty($image = $club->images->first())) ? $image->source : url('img/noimage.png') }}) 50% 0% no-repeat; background-size: cover;"> </span>
                        	</a>
                        	<div class="tags">
	                            <span class="label-holder">
	                                <span class="label label-info">{{ $club->title }}</span>
	                            </span>
                        	</div>
                        	<div class="tools tools-top in">
                            	<a href="{{ URL::to('admin/main/edit/' . $club->id) }}">
                                	<i class="ace-icon fa fa-pencil"></i>
                            	</a>
                            	<a href="{{ URL::to('admin/main/delete/' . $club->id) }}" class="delete-object">
                                	<i class="ace-icon fa fa-trash-o"></i>
                            	</a>
                        	</div>
        				</li>
        			@endforeach
        			--}}
        		 -->
        		</ul>
        	</div>
		</div>
	</div>
</div>
@endsection