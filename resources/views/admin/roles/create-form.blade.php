<div class="page-content">
	<div class="col-sm-6">
		<div class="widget-box">
			<div class="widget-header">
				<h4 class="widget-title">Новая пользовательская роль</h4>
			</div>
			<div class="widget-body">
			{{ Form::open(['class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
				<div class="widget-main">
				
					<div class="row">
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Наименование</label>
						<div class="col-sm-8">
							{!! Form::text('name', null, array('id' => 'form-field-username', 'placeholder' => 'Наименование', 'class' => 'col-xs-12' )) !!}
							@foreach($errors->get('name') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Код</label>
						<div class="col-sm-8">
							{!! Form::text('slug', null, array('id' => 'form-field-password', 'placeholder' => 'Код', 'class' => 'col-xs-12' )) !!}
							@foreach($errors->get('slug') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
					</div>
					@if ($type == 'permission')
					
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Модель</label>
						<div class="col-sm-8">
							{!! Form::text('model', null, array('id' => 'form-field-password', 'placeholder' => 'Модель', 'class' => 'col-xs-12' )) !!}
							@foreach($errors->get('model') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
					</div>
					@endif
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Описание</label>
						<div class="col-sm-8">
							{!! Form::textarea('description', null, array('id' => 'form-field-password_confirmation', 'placeholder' => 'Описание', 'class' => 'col-xs-12' )) !!}
							@foreach($errors->get('description') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
					</div>
					@if ($type == 'role')
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Родительская роль</label>
						<div class="col-sm-8">
							<select name="parent">
									<option>../</option>
								@foreach ($groups as $group)
									<option value="{{ $group->id }}">{{ $group->slug }}</option>
								@endforeach
							</select>
							@foreach($errors->get('parent') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
					</div>
					@endif
					</div>
					   
				
				</div>
				<div class="widget-main no-padding">
					<div class="form-actions center">
						<button type="submit" class="btn btn-sm btn-success">Сохранить</button>
						<button type="reset" class="btn btn-sm">Очистить</button>
					</div>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>