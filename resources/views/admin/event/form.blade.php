<div class="page-content">
	{{ Form::model($model, ['class'=> 'form-horizontal' , 'url' => 'admin/events/edit/' . $model->id, 'enctype' => 'multipart/form-data']) }}    
        

        

<h4 class="header blue bolder smaller">Карточка события</h4>

<div class="form-group {{ (($errors->has('title'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Название</label>
    <div class="col-sm-3">
    	{!! Form::text('title', null, array('id' => 'form-field-username', 'placeholder' => 'Название', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>

<div class="form-group {{ (($errors->has('club_id'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-club">Клуб</label>
    <div class="col-sm-3">
        <select class="form-control" id="form-field-club" name="club_id">
        	
            <option value="">Не выбран ...</option>
            @foreach ($clubs as $club)
            	<option value="{{ $club->id }}" {{ ($model->club_id == $club->id) ? 'selected' : '' }}>{{ $club->title }}</option>
            @endforeach
        </select>
        @foreach($errors->get('club_id') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
@if ($type == 'event' || $type == 'action')
<div class="form-group {{ (($errors->has('scheduled_at'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Дата начала</label>
    <div class="col-sm-3">
        <div class="input-group">
        	{!! Form::text('scheduled_at', null, array('id' => 'id-date-picker-1', 'placeholder' => 'Дата начала', 'class' => 'form-control date-picker')) !!}
            <span class="input-group-addon">
                <i class="fa fa-calendar bigger-110"></i>
            </span>
        </div>
        @foreach($errors->get('scheduled_at') as $message)  
		    	<div class="alert alert-danger">
		    		{{ $message }}
		    	</div>
	    @endforeach
    </div>
</div>


<div class="form-group {{ (($errors->has('end_at'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Дата окончания</label>
    <div class="col-sm-3">
        <div class="input-group">
        	{!! Form::text('end_at', null, array('id' => 'id-date-picker-2', 'placeholder' => 'Дата окончания', 'class' => 'form-control date-picker')) !!}
            <span class="input-group-addon">
                <i class="fa fa-calendar bigger-110"></i>
            </span>
        </div>
        @foreach($errors->get('end_at') as $message)  
		    	<div class="alert alert-danger">
		    		{{ $message }}
		    	</div>
	    @endforeach
    </div>
</div>
@endif

<div class="form-group {{ (($errors->has('published_at'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Дата публикации</label>
    <div class="col-sm-3">
        <div class="input-group">
        	{!! Form::text('published_at', null, array('id' => 'id-date-picker-3', 'placeholder' => 'Дата публикации', 'class' => 'form-control date-picker')) !!}
            <span class="input-group-addon">
                <i class="fa fa-calendar bigger-110"></i>
            </span>
        </div>
        @foreach($errors->get('published_at') as $message)  
		    	<div class="alert alert-danger">
		    		{{ $message }}
		    	</div>
	    @endforeach
    </div>
</div>
@if ($type == 'action')
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-link">Акции в топе</label>
    <div class="col-sm-6">
    	{!! Form::checkbox('on_top', null, ($model->on_top == 1)) !!}
    </div>
</div>
@endif
@if ($type == 'action' || $type == 'event')
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-link">Ссылка на покупку</label>
    <div class="col-sm-6">
    	{!! Form::text('buy_link', null, array('id' => 'form-field-link', 'placeholder' => 'Ссылка на покупку', 'class' => 'col-xs-12')) !!}
    </div>
</div>
@endif
@if ($type == 'action')
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-about">Текст превью</label>
    <div class="col-sm-6">
    	{!! Form::textarea('preview_text', null, array('placeholder' => 'Текст превью', 'class' => 'col-xs-12', 'row'=>5)) !!}
    </div>
</div>
@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-about">Описание</label>
    <div class="col-sm-6">
    	{!! Form::textarea('detail_text', null, array('placeholder' => 'Детальный текст', 'class' => 'col-xs-12', 'row'=>5)) !!}
    </div>
</div>
<h4 class="header blue bolder smaller">Видео</h4>

@if (count($model->videos))
@php
	$sorce = parse_url($model->videos[0]->source);
	$query = null;
	if (isset($sorce["query"]))
		parse_str($sorce["query"], $query);
@endphp
<iframe width="854" height="480" src="{{ (isset($query['v'])) ? 'https://www.youtube.com/embed/' .  $query['v'] : $model->videos[0]->source }}" frameborder="0" allowfullscreen></iframe>
@endif
<div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Ссылка на видео youtube</label>
						<div class="col-sm-6">
                        	{!! Form::text('video[source]', (count($model->videos)) ? $model->videos[0]->source : null, array('placeholder' => 'Ссылка на видео', 'class' => 'width-100')) !!}
                        </div>
                    </div>
                    <!-- 
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Название</label>
						<div class="col-sm-6">
                        	{!! Form::text('video[title]', (count($model->videos)) ? $model->videos[0]->title : null, array('placeholder' => 'Название', 'class' => 'width-100')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-about">Описание видео</label>
                        <div class="col-sm-6">
                        	{!! Form::textarea('video[description]', (count($model->videos)) ? $model->videos[0]->description : null, array('placeholder' => 'Описание', 'class' => 'col-xs-12', 'row'=>5)) !!}
                        </div>
                    </div>
 					-->
<h4 class="header blue bolder smaller">Загрузить фото</h4>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Фото</label>

    <div class="col-sm-3">
        <input type="file" name="files[]" multiple>
    </div>
</div>
<div class="space"></div>
                    <h4 class="header blue bolder smaller">Фотографии</h4>
                    <ul class="ace-thumbnails clearfix">
					@foreach ($model->images as $image)
                    
                        <li>
                            <a href="{{ $image->source }}" id="img_{{ $image->id }}" title="{{ $image->title }}" data-rel="colorbox" class="cboxElement">
                                <img width="150" height="150" alt="150x150" src="{{ $image->source }}">
                            </a>

                            <div class="tools tools-top in">
                                <a href="{{ URL::to('admin/file/remove/' . $image->id ) }}" class="remove_photo">
                                    <i class="ace-icon fa fa-times red"></i>
                                </a>
                            </div>
                            <input type="hidden" name="images[]" id="file_{{ $image->id }}" value="{{ $image->id }}" />
                        </li>
                    @endforeach
                    </ul>
<div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-info">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Опубликовать
                </a>

                &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Очистить
                </button>
            </div>
        </div>
    {{ Form::close() }}
</div>