<div class="page-content">
	{{ Form::open(['class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}    
        

        

<h4 class="header blue bolder smaller">Карточка события</h4>

<div class="form-group {{ (($errors->has('title'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Название</label>
    <div class="col-sm-3">
    	{!! Form::text('title', null, array('id' => 'form-field-username', 'placeholder' => 'Название', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>

<div class="form-group {{ (($errors->has('club_id'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-club">Клуб</label>
    <div class="col-sm-3">
        <select class="form-control" id="form-field-club" name="club_id">
        	
            <option value="">Не выбран ...</option>
            @foreach ($clubs as $club)
            	<option value="{{ $club->id }}">{{ $club->title }}</option>
            @endforeach
        </select>
        @foreach($errors->get('club_id') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
@if ($type == 'event' || $type == 'action')
<div class="form-group {{ (($errors->has('scheduled_at'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Дата начала</label>
    <div class="col-sm-3">
        <div class="input-group">
        	{!! Form::text('scheduled_at', null, array('id' => 'id-date-picker-1', 'placeholder' => 'Дата начала', 'class' => 'form-control date-picker')) !!}
            <span class="input-group-addon">
                <i class="fa fa-calendar bigger-110"></i>
            </span>
        </div>
        @foreach($errors->get('scheduled_at') as $message)  
		    	<div class="alert alert-danger">
		    		{{ $message }}
		    	</div>
	    @endforeach
    </div>
</div>


<div class="form-group {{ (($errors->has('end_at'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Дата окончания</label>
    <div class="col-sm-3">
        <div class="input-group">
        	{!! Form::text('end_at', null, array('id' => 'id-date-picker-2', 'placeholder' => 'Дата окончания', 'class' => 'form-control date-picker')) !!}
            <span class="input-group-addon">
                <i class="fa fa-calendar bigger-110"></i>
            </span>
        </div>
        @foreach($errors->get('end_at') as $message)  
		    	<div class="alert alert-danger">
		    		{{ $message }}
		    	</div>
	    @endforeach
    </div>
</div>
@endif

<div class="form-group {{ (($errors->has('published_at'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Дата публикации</label>
    <div class="col-sm-3">
        <div class="input-group">
        	{!! Form::text('published_at', null, array('id' => 'id-date-picker-3', 'placeholder' => 'Дата публикации', 'class' => 'form-control date-picker')) !!}
            <span class="input-group-addon">
                <i class="fa fa-calendar bigger-110"></i>
            </span>
        </div>
        @foreach($errors->get('published_at') as $message)  
		    	<div class="alert alert-danger">
		    		{{ $message }}
		    	</div>
	    @endforeach
    </div>
</div>
@if ($type == 'action')
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-link">Акции в топе</label>
    <div class="col-sm-6">
    	{!! Form::checkbox('on_top', null, array()) !!}
    </div>
</div>
@endif
@if ($type == 'action' || $type == 'event')
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-link">Ссылка на покупку</label>
    <div class="col-sm-6">
    	{!! Form::text('buy_link', null, array('id' => 'form-field-link', 'placeholder' => 'Ссылка на покупку', 'class' => 'col-xs-12')) !!}
    </div>
</div>
@endif
@if ($type == 'action')
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-about">Текст превью</label>
    <div class="col-sm-6">
    	{!! Form::textarea('preview_text', null, array('placeholder' => 'Текст превью', 'class' => 'col-xs-12', 'row'=>5)) !!}
    </div>
</div>
@endif
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-about">Описание</label>
    <div class="col-sm-6">
    	{!! Form::textarea('detail_text', null, array('placeholder' => 'Детальный текст', 'class' => 'col-xs-12', 'row'=>5)) !!}
    </div>
</div>
<h4 class="header blue bolder smaller">Видео</h4>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Ссылка на видео youtube</label>
						<div class="col-sm-6">
                        	{!! Form::text('video[source]', null, array('placeholder' => 'Ссылка на видео', 'class' => 'width-100')) !!}
                        </div>
                    </div>
                    <!-- 
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Название</label>
						<div class="col-sm-6">
                        	{!! Form::text('video[title]', null, array('placeholder' => 'Название', 'class' => 'width-100')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-about">Описание видео</label>
                        <div class="col-sm-6">
                        	{!! Form::textarea('video[description]', null, array('placeholder' => 'Описание', 'class' => 'col-xs-12', 'row'=>5)) !!}
                        </div>
                    </div>
                     -->

<h4 class="header blue bolder smaller">Загрузить фотографии</h4>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Фото</label>

    <div class="col-sm-3">
        <input type="file" name="files[]" multiple>
    </div>
</div>

<div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-info">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Опубликовать
                </a>

                &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Очистить
                </button>
            </div>
        </div>
    {{ Form::close() }}
</div>