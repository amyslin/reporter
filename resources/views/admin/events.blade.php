@extends('layouts.admin')

@section('title', 'Index')

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
        	<a href="{{ url('admin/events/create/' . $type) }}" class="btn btn-xs btn-success">
                <i class="ace-icon fa fa-plus bigger-120"></i> Добавить событие
         	</a>	

            <div class="space-6"></div>
        	<div>
        		<ul class="ace-thumbnails clearfix">
        			@foreach ($events as $event)
        				<li>
        					<a href="{{ URL::to('admin/events/edit/' . $event->id) }}" data-rel="colorbox">
        						<span style="display: block; width: 320px; height: 255px; background: url({{ (!empty($image = $event->images->first())) ? $image->source : url('img/noimage.png') }}) 50% 0% no-repeat; background-size: cover;"> </span>
                            	<!-- <img width="320" src="{{ (!empty($image = $event->images->first())) ? $image->source : url('img/noimage.png') }}" /> -->
                            	<div class="text">
                            		<div class="inner">{{ $event->title }}</div>
                        		</div>
                        	</a>
                        	<div class="tags">
	                            <span class="label-holder">
	                                <span class="label label-info">{{ $event->title }}</span>
	                            </span>
                        	</div>
                        	<div class="tools in">
                            	<a href="{{ URL::to('admin/events/edit/' . $event->id) }}">
                                	<i class="ace-icon fa fa-pencil"></i>
                            	</a>
                            	<a href="{{ URL::to('admin/events/delete/' . $event->id) }}" class="delete-object">
                                	<i class="ace-icon fa fa-trash-o"></i>
                            	</a>
                        	</div>
        				</li>
        			@endforeach
        		</ul>
        	</div>
		</div>
	</div>
</div>
@endsection