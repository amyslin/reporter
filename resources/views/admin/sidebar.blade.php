<!-- #section:basics/sidebar -->
<div id="sidebar" class="sidebar responsive ace-save-state">
    <script type="text/javascript">
        try{ace.settings.loadState('sidebar')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li>
            <a href="{{ url('admin/main') }}">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text">Главная</span>
            </a>
        </li>

        <li>
            <a href="{{ url('admin/users') }}">
                <i class="menu-icon fa fa-group"></i>
                <span class="menu-text">Пользователи</span>
            </a>
        </li>
        
        <li>
            <a href="{{ url('admin/roles') }}">
                <i class="menu-icon glyphicon glyphicon-lock"></i>
                <span class="menu-text">Доступ</span>
            </a>
        </li>
        <li>
        	<a href="{{ url('admin/categories') }}">
        		<i class="menu-icon fa fa-folder-open"></i>
        		<span class="menu-text">Категории</span>
        	</a>
        </li>
		<li>
        	<a href="{{ url('admin/pages') }}">
        		<i class="menu-icon fa fa-list"></i>
        		<span class="menu-text">Публикации</span>
        	</a>
        </li>
        <li>
        	<a href="{{ url('admin/forms') }}">
        		<i class="menu-icon fa fa-pencil-square-o"></i>
        		<span class="menu-text">Формы</span>
        	</a>
        </li>
        <li>
        	<a href="{{ url('admin/dictionaries') }}">
        		<i class="menu-icon fa fa-list"></i>
        		<span class="menu-text">Справочники форм</span>
        	</a>
        </li>
        <li>
        	<a href="#" class="dropdown-toggle">
        		<i class="menu-icon fa fa-globe"></i>
        		<span class="menu-text">Сайт</span>
        	</a>
        	<b class="arrow"></b>
        	<ul class="submenu nav-show" style="display: none;">
        		<li>
        			<a href="{{ url('admin/menu') }}">
        				<i class="menu-icon fa fa-caret-right"></i>
        				Меню
        			</a>
        		</li>
        	</ul>
        </li>
        <li>
            <a href="#" onclick="alert('Ведёт на appboy')">
                <i class="menu-icon fa fa-envelope-o"></i>
                <span class="menu-text">Рассылки</span>
            </a>
        </li>
        
		<!-- 
        <li>
            <a href="#">
                <i class="menu-icon fa fa-comments-o"></i>
                <span class="menu-text">Обращения</span>
            </a>
        </li>
         -->

    </ul><!-- /.nav-list -->

    <!-- #section:basics/sidebar.layout.minimize -->
    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>

    <!-- /section:basics/sidebar.layout.minimize -->
</div>
