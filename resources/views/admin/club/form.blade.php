<div class="page-content">
	{{ Form::model($model, ['class'=> 'form-horizontal' , 'url' => 'admin/main/edit/' . $model->id]) }}
		{{ Form::hidden('loc_lat', null) }}
		{{ Form::hidden('loc_lon', null) }}
        <div class="tabbable">
            <ul class="nav nav-tabs padding-16">
                <li class="active">
                    <a data-toggle="tab" href="#edit-basic" aria-expanded="true">
                        <i class="green ace-icon fa fa-pencil-square-o bigger-125"></i>
                        Основное
                    </a>
                </li>
				<!-- 
                <li class="">
                    <a data-toggle="tab" href="#edit-settings" aria-expanded="false">
                        <i class="purple ace-icon fa fa-image bigger-125"></i>
                        Фотогалерея
                    </a>
                </li>
                 -->
            </ul>

            <div class="tab-content profile-edit-tab-content">
                <div id="edit-basic" class="tab-pane active">
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Название</label>
                        <div class="col-sm-3">
                        	{!! Form::text('title', null, array('placeholder' => 'Название', 'class' => 'col-xs-12')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-phone">Телефон</label>
                        <div class="col-sm-3">
                            <span class="input-icon input-icon-right width-100">
                            	{!! Form::text('phone', null, array('placeholder' => 'Телефон', 'class' => 'width-100 input-mask-phone')) !!}
                                <i class="ace-icon fa fa-phone fa-flip-horizontal"></i>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-address">Адрес</label>
                        <div class="col-sm-6">
                            <div class="input-group">
                            	{!! Form::text('address', null, array('placeholder' => 'Адрес', 'class' => 'col-xs-12')) !!}
                                <span class="input-group-btn">
                                    <button class="btn btn-sm btn-default geocode" type="button">
                                        <i class="ace-icon fa fa-map-marker bigger-110"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
					
					<div class="form-group">
                    	<label class="col-sm-3 control-label no-padding-right" for="form-field-address">Карта</label>
                    	<div class="col-sm-6">
                    		<div id="yaMap" style="height: 400px"></div>
                    	
                    	</div>
                   	</div>
                   	
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right" for="form-field-about">О клубе</label>
                        <div class="col-sm-6">
                        	{!! Form::textarea('detail', null, array('placeholder' => 'Описание', 'class' => 'col-xs-12', 'row'=>5)) !!}
                        </div>
                    </div>
                    
                    
                    
                    <h4 class="header blue bolder smaller">Загрузить фото</h4>

                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-username">Фото</label>

                        <div id="file-inputs" class="col-sm-3">
                        	<input name="images[]" type="file" multiple />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button class="btn btn-info upload-btn" type="button">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Загрузить
                            </button>
                        </div>
                    </div>

                    <div class="space"></div>
                    <h4 class="header blue bolder smaller">Фотографии</h4>
                    <ul class="ace-thumbnails clearfix">
					@foreach ($model->images as $image)
                    
                        <li>
                            <a href="{{ $image->source }}" id="img_{{ $image->id }}" title="{{ $image->title }}" data-rel="colorbox" class="cboxElement">
                                <img width="150" height="150" alt="150x150" src="{{ $image->source }}">
                            </a>

                            <div class="tools tools-top in">
                                <a href="{{ URL::to('admin/file/remove/' . $image->id ) }}" class="remove_photo">
                                    <i class="ace-icon fa fa-times red"></i>
                                </a>
                            </div>
                            <input type="hidden" name="files[]" id="file_{{ $image->id }}" value="{{ $image->id }}" />
                        </li>
                    @endforeach
                    </ul>
                </div>

                <div id="edit-settings" class="tab-pane">
                	<h4 class="header blue bolder smaller">Видео</h4>
@if (count($model->videos))
@php
	$sorce = parse_url($model->videos[0]->source);
	$query = null;
	if (isset($sorce["query"]))
		parse_str($sorce["query"], $query);
@endphp
<iframe width="854" height="480" src="{{ (isset($query['v'])) ? 'https://www.youtube.com/embed/' .  $query['v'] : $model->videos[0]->source }}" frameborder="0" allowfullscreen></iframe>
@endif
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-username">Ссылка на видео youtube</label>
						<div class="col-sm-6">
                        	{!! Form::text('video[source]', (count($model->videos)) ? $model->videos[0]->source : null, array('placeholder' => 'Ссылка на видео', 'class' => 'width-100')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-username">Название</label>
						<div class="col-sm-6">
                        	{!! Form::text('video[title]', (count($model->videos)) ? $model->videos[0]->title : null, array('placeholder' => 'Название', 'class' => 'width-100')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-about">Описание видео</label>
                        <div class="col-sm-6">
                        	{!! Form::textarea('video[description]', (count($model->videos)) ? $model->videos[0]->description : null, array('placeholder' => 'Описание', 'class' => 'col-xs-12', 'row'=>5)) !!}
                        </div>
                    </div>
                </div>
                
                

                <div id="edit-menu" class="tab-pane">
                    <h4 class="header blue bolder smaller">Добавить в меню</h4>

                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-name">Название</label>

                        <div class="col-sm-3">
                            <input class="col-xs-12" type="text" id="form-field-name" placeholder="Название" value="Шот водки">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-select-1">Категория</label>

                        <div class="col-sm-10">
                            <div class="width-100 btn-group">
                                <button data-toggle="dropdown" class="col-sm-2 btn btn-white dropdown-toggle">
                                    <span>Бар</span>
                                    <i class="ace-icon fa fa-angle-down icon-on-right"></i>
                                </button>

                                <ul class="dropdown-menu">
                                    <li><a href="#">Бар</a></li>
                                    <li><a href="#">Закуски</a></li>
                                    <li><a href="#">Китайская кухня</a></li>
                                    <li><a href="#">Японская кухня</a></li>
                                </ul>

                                <button class="btn btn-success btn-white">
                                    <i class="ace-icon fa fa-plus icon-on-center"></i>
                                </button>

                                <button class="btn btn-primary btn-white">
                                    <i class="ace-icon fa fa-pencil"></i>
                                </button>

                                <button class="btn btn-danger btn-white">
                                    <i class="ace-icon fa fa-ban"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button class="btn btn-info" type="button">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Добавить
                            </button>
                        </div>
                    </div>

                    <div class="space"></div>
                    <h4 class="header blue bolder smaller">Список меню</h4>

                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Категория</th>
                            <th>Название</th>
                            <th class="text-right">Стоимость, руб.</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="records">
                        <tr>
                            <td>Бар</td>
                            <td>Шот водки</td>
                            <td class="text-right">100</td>
                            <td class="text-right">
                                <a href="#" title="" class="btn btn-xs btn-success">
                                    <i class="ace-icon fa fa-check"></i>
                                </a>
                                <a href="#" title="" class="btn btn-xs btn-primary">
                                    <i class="ace-icon fa fa-pencil"></i>
                                </a>
                                <a href="#" title="" class="btn btn-xs btn-danger">
                                    <i class="ace-icon fa fa-ban"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Бар</td>
                            <td>Б52</td>
                            <td class="text-right">200</td>
                            <td class="text-right">
                                <a href="#" title="" class="btn btn-xs btn-success">
                                    <i class="ace-icon fa fa-check"></i>
                                </a>
                                <a href="#" title="" class="btn btn-xs btn-primary">
                                    <i class="ace-icon fa fa-pencil"></i>
                                </a>
                                <a href="#" title="" class="btn btn-xs btn-danger">
                                    <i class="ace-icon fa fa-ban"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>Закуски</td>
                            <td>Арахис</td>
                            <td class="text-right">200</td>
                            <td class="text-right">
                                <a href="#" title="" class="btn btn-xs btn-success">
                                    <i class="ace-icon fa fa-check"></i>
                                </a>
                                <a href="#" title="" class="btn btn-xs btn-primary">
                                    <i class="ace-icon fa fa-pencil"></i>
                                </a>
                                <a href="#" title="" class="btn btn-xs btn-danger">
                                    <i class="ace-icon fa fa-ban"></i>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div id="edit-calendar" class="tab-pane">
                    <h4 class="header blue bolder smaller">Добавить шоу-программу</h4>

                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-name">Название</label>

                        <div class="col-sm-3">
                            <input class="col-xs-12" type="text" id="form-field-name" placeholder="Название" value="Ковбойская ночь">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label no-padding-right" for="form-field-select-1">Дни</label>

                        <div class="col-sm-3 radio">
                            <label class="inline">
                                <input name="calendar-type" type="radio" class="ace" checked="checked" value="weekday">
                                <span class="lbl middle"> Дни недели</span>
                            </label>

                            &nbsp;
                            <label class="inline">
                                <input name="calendar-type" type="radio" class="ace" value="date">
                                <span class="lbl middle"> Дата</span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-7 col-sm-offset-2 radio" id="calendar-weekday">
                            <label class="inline">
                                <input name="form-field-weekday" type="checkbox" class="ace">
                                <span class="lbl middle"> Пн</span>
                            </label>

                            &nbsp;
                            <label class="inline">
                                <input name="form-field-weekday" type="checkbox" class="ace">
                                <span class="lbl middle"> Вт</span>
                            </label>

                            &nbsp;
                            <label class="inline">
                                <input name="form-field-weekday" type="checkbox" class="ace">
                                <span class="lbl middle"> Ср</span>
                            </label>

                            &nbsp;
                            <label class="inline">
                                <input name="form-field-weekday" type="checkbox" class="ace" checked="checked">
                                <span class="lbl middle"> Чт</span>
                            </label>

                            &nbsp;
                            <label class="inline">
                                <input name="form-field-weekday" type="checkbox" class="ace">
                                <span class="lbl middle"> Пт</span>
                            </label>

                            &nbsp;
                            <label class="inline">
                                <input name="form-field-weekday" type="checkbox" class="ace">
                                <span class="lbl middle"> Сб</span>
                            </label>

                            &nbsp;
                            <label class="inline">
                                <input name="form-field-weekday" type="checkbox" class="ace">
                                <span class="lbl middle"> Вс</span>
                            </label>
                        </div>

                        <div class="col-sm-3 col-sm-offset-2" id="calendar-date" style="display:none">
                            <div class="input-group">
                                <input class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar bigger-110"></i>
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <button class="btn btn-info" type="button">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                Добавить
                            </button>
                        </div>
                    </div>

                    <div class="space"></div>
                    <h4 class="header blue bolder smaller">Все шоу-программы</h4>

                    <div id="calendar"></div>
                </div>
            </div>
        </div>

        <div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button class="btn btn-info" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Сохранить
                </button>

                &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Очистить
                </button>
            </div>
        </div>
    {{ Form::close() }}
</div>

<script type="text/javascript">
	var map;
	function initMap (ymaps) {  
    	map = new ymaps.Map("yaMap", {
          center: [{{ (!empty($model->loc_lat))?$model->loc_lat:55.76 }}, {{ (!empty($model->loc_lon))?$model->loc_lon:37.64 }}], 
          zoom: 15
        });
    	map.geoObjects.add(new ymaps.Placemark([{{ (!empty($model->loc_lat))?$model->loc_lat:55.76 }}, {{ (!empty($model->loc_lon))?$model->loc_lon:37.64 }}]));
  	}
</script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=initMap&csp=true" type="text/javascript"></script>