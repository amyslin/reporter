@extends('layouts.admin')

@section('title', 'Создать проект')

@section('content')

	@include('admin.club.create-form')
	
@endsection
