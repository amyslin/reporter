@extends('layouts.admin')

@section('title', 'Index')

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
        	<a href="{{ url('admin/main/create') }}" class="btn btn-xs btn-success">
                <i class="ace-icon fa fa-plus bigger-120"></i> Добавить сущность
         	</a>
         	<div class="space-6"></div>
        	<div>
        		<ul class="ace-thumbnails clearfix">
        		<!--
        			{{-- 
        			@foreach ($clubs as $club)
        				<li>
        					<a href="{{ URL::to('admin/main/edit/' . $club->id) }}" data-rel="colorbox">
        						<span style="display: block; width: 180px; height: 255px; background: url({{ (!empty($image = $club->images->first())) ? $image->source : url('img/noimage.png') }}) 50% 0% no-repeat; background-size: cover;"> </span>
                        	</a>
                        	<div class="tags">
	                            <span class="label-holder">
	                                <span class="label label-info">{{ $club->title }}</span>
	                            </span>
                        	</div>
                        	<div class="tools tools-top in">
                            	<a href="{{ URL::to('admin/main/edit/' . $club->id) }}">
                                	<i class="ace-icon fa fa-pencil"></i>
                            	</a>
                            	<a href="{{ URL::to('admin/main/delete/' . $club->id) }}" class="delete-object">
                                	<i class="ace-icon fa fa-trash-o"></i>
                            	</a>
                        	</div>
        				</li>
        			@endforeach
        			--}}
        		 -->
        		</ul>
        	</div>
		</div>
	</div>
</div>
@endsection