@extends('layouts.admin')

@section('title', 'Добавить страницу')

@section('content')

	@include('admin.page.create-form')
	
@endsection