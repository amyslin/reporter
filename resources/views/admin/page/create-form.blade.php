<div class="page-content">
	{{ Form::open(['class'=> 'form-horizontal', 'id'=>'form-editor' , 'url' => 'admin/page/create', 'enctype' => 'multipart/form-data']) }}    
        

        

<h4 class="header blue bolder smaller">Создание страницы</h4>

<div class="form-group {{ (($errors->has('title'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Название</label>
    <div class="col-sm-3">
    	{!! Form::text('title', null, array('placeholder' => 'Название', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Тип</label>
						<div class="col-sm-8">
							<select name="type">
								<option value="page">Страница</option>
								<option value="blog">Блог</option>
								
							</select>
							@foreach($errors->get('type') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
</div>
<div class="form-group {{ (($errors->has('code'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Код страницы</label>
    <div class="col-sm-3">
    	{!! Form::text('code', null, array('placeholder' => 'Код страницы', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('code') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right">Категория</label>
						<div class="col-sm-3">
							<select multiple="" class="chosen-select form-control" id="form-field-select-4" data-placeholder="Выбирите категории..." name="categories[]">
								@foreach($categories as $item)
									<option value="{{ $item->id }}">{{ $item->title }}</option>
								@endforeach
							</select>
							@foreach($errors->get('type') as $message)  
						    	<div class="alert alert-danger">
						    		{{ $message }}
						    	</div>
						    @endforeach
						</div>
</div>

<div class="form-group {{ (($errors->has('published_at'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Публикация от</label>
    <div class="col-sm-3">
    	<div class="input-group">
        	{!! Form::text('published_at', null, array('id' => 'id-date-picker-2', 'placeholder' => 'Публикация от', 'class' => 'form-control date-picker')) !!}
            <span class="input-group-addon">
                <i class="fa fa-calendar bigger-110"></i>
            </span>
        </div>
	    @foreach($errors->get('published_at') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
{{--
<div class="form-group {{ (($errors->has('active'))? 'has-error' : '') }}">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-username">Активность</label>
	<label class="middle">
		{!! Form::checkbox('active', null, ['class' => 'ace', 'value' => 1]) !!}
	</label>
</div>
--}}
{{--
<div class="form-group {{ (($errors->has('view'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Путь к файлу шаблона</label>
    <div class="col-sm-3">
    	{!! Form::text('view', null, array('placeholder' => 'Путь к файлу шаблона', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
<div class="form-group {{ (($errors->has('controller'))? 'has-error' : '') }}">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-username">Контроллер</label>
    <div class="col-sm-3">
    	{!! Form::text('controller', null, array('placeholder' => 'Контроллер', 'class' => 'col-xs-12' )) !!}
	    @foreach($errors->get('title') as $message)  
	    	<div class="alert alert-danger">
	    		{{ $message }}
	    	</div>
	    @endforeach
    </div>
</div>
--}}
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-about">Анонс</label>
    <div class="col-sm-9">
<!--     	<div id="editor" class="wysiwyg-editor"></div> -->
    	{!! Form::textarea('preview_text', null, []) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-about">Содержание</label>
    <div class="col-sm-9">
    	<div id="editor" class="wysiwyg-editor"></div>
    	{!! Form::hidden('content', null, array('id' => 'editor-value')) !!}
    </div>
</div>

<h4 class="header blue bolder smaller">Фотография превьюхи</h4>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" >Фото</label>

    <div class="col-sm-3">
        <input type="file" name="preview_img">
    </div>
</div>
<h4 class="header blue bolder smaller">Фотография просмотра</h4>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" >Фото</label>

    <div class="col-sm-3">
        <input type="file" name="full_img">
    </div>
</div>

<div class="clearfix form-actions">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-info">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Сохранить
                </a>

                &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Очистить
                </button>
            </div>
        </div>
    {{ Form::close() }}
</div>