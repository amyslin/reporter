@extends('layouts.admin')

@section('title', 'Меню')

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
        
        	<a href="{{ url('admin/menu/create') }}" class="btn btn-xs btn-success">
                <i class="ace-icon fa fa-plus bigger-120"></i> Добавить пункт
         	</a>
         	
         
		</div>
	</div>
	<div class="row">
	<div class="col-sm-6">
	        	<div class="widget-box widget-color-green2">
											<div class="widget-header">
												<h4 class="widget-title lighter smaller">
													Меню сайта
												</h4>
											</div>

											<div class="widget-body">
												<div class="widget-main padding-8">
													<ul id="menu_tree"></ul>
												</div>
											</div>
				</div>
	</div>
	</div>
</div>

<script type="text/javascript">
<!--
<?php 
$tree = \App\Facades\Menu::adminTree(); 
$data = [];
foreach($tree as $key => $place) {
	$data[$key] = ['text' => $key . '<div class="pull-right hidden-sm action-buttons"><a href="/admin/menu/create?place='.$key.'" class="green"><i class="ace-icon fa fa-plus bigger-130"></i></a></div>', 'type' => 'folder', 'icon-class' => 'blue'];
	$childrens = [];
		
	$data[$key]['additionalParameters']['children'] = getTree($place);
	
}

function getTree($group) {
	$itemIcon = [
			null => 'fa-file',
			'page' => 'fa-file-text-o',
			'controller' => 'fa-gears',
			'view' => 'fa-file-code-o',
			'link' => 'fa-link',
	];
	$tree = [];
	foreach ($group as $key => $item) {
		if (isset($item['childs'])) {
			$tree[$key] = [
					'text' => '<i class="ace-icon fa '.$itemIcon[$item['type']].' green"></i>  ' 
								. $item['title'] . " ({$item['type']})" 
	         					. '<div class="pull-right hidden-sm action-buttons">' 
	         					. '<a href="/admin/menu/create?parent='.$item['id'].'" class="green"><i class="ace-icon fa fa-plus bigger-130"></i></a>' 
	         					. '<a href="/admin/menu/edit/' . $item['id'] .'" class="green"><i class="ace-icon fa fa-pencil bigger-130"></i></a>' 
	         					. '<a href="/admin/menu/delete/' . $item['id'] . '" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>' 
	         					. '</div>',
					'type' => 'folder'
			];
			$tree[$key]['additionalParameters']['children'] = getTree($item['childs']);
		}
		else {
			$tree[$key] = [
					'text' => '<i class="ace-icon fa '.$itemIcon[$item['type']].' green"></i>  '
					. $item['title'] . " ({$item['type']})"
					. '<div class="pull-right hidden-sm action-buttons">'
	         		. '<a href="/admin/menu/create?parent='.$item['id'].'" class="green"><i class="ace-icon fa fa-plus bigger-130"></i></a>'
	         		. '<a href="/admin/menu/edit/' . $item['id'] .'" class="green"><i class="ace-icon fa fa-pencil bigger-130"></i></a>'
	         		. '<a href="/admin/menu/delete/' . $item['id'] . '" class="red"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>'
	         		. '</div>',
					'type' => 'item'
			];
		}
	}
	return $tree;
}

?>


//-->
var tree_data = <?= json_encode($data); ?>;

var dataSource = function(options, callback){
	var $data = null
	if(!("text" in options) && !("type" in options)){
		$data = tree_data;//the root tree
		callback({ data: $data });
		return;
	}
	else if("type" in options && options.type == "folder") {
		if("additionalParameters" in options && "children" in options.additionalParameters)
			$data = options.additionalParameters.children || {};
		else $data = {}//no data
	}
	
	if($data != null)//this setTimeout is only for mimicking some random delay
		setTimeout(function(){callback({ data: $data });} , parseInt(Math.random() * 500) + 200);

	//we have used static data here
	//but you can retrieve your data dynamically from a server using ajax call
	//checkout examples/treeview.html and examples/treeview.js for more info
}



$(document).ready(function() {
	console.log( "document loaded" );
	$('#menu_tree').ace_tree({
		dataSource: dataSource ,
		loadingHTML:'<div class="tree-loading"><i class="ace-icon fa fa-refresh fa-spin blue"></i></div>',
		'open-icon' : 'ace-icon fa fa-folder-open',
		'close-icon' : 'ace-icon fa fa-folder',
		'itemSelect' : false,
		'folderSelect': false,
		'multiSelect': false,
		'selected-icon' : null,
		'unselected-icon' : null,
		'folder-open-icon' : 'ace-icon tree-plus',
		'folder-close-icon' : 'ace-icon tree-minus'
	});
});
</script>
@endsection