@extends('layouts.admin')

@section('title', 'Страницы')

@section('content')
<div class="page-content">
    <div class="row">
        <div class="col-xs-12">
        
        	<a href="{{ url('admin/page/create') }}" class="btn btn-xs btn-success">
                <i class="ace-icon fa fa-plus bigger-120"></i> Добавить страницу
         	</a>
         	
         	
         	
         	<div class="space-6"></div>
         	
         	
         
	         	<table class="table  table-bordered table-hover">
	         		<thead>
	         			<th>
	         				
	         			</th>
	         			<th>
	         				Заголовок
	         			</th>
	         			<th>
	         				Тип
	         			</th>
	         			<th>
	         				Код
	         			</th>
	         			<th>
	         				Дата публикации
	         			</th>
	         			<th>
	         				Дата создания
	         			</th>
	         			<th>
	         				Дата модификации
	         			</th>
	         			<th>
	         			
	         			</th>
	         		</thead>
	         		<tbody>
	         			@foreach ($models as $model)
	         			<tr>
	         				<td>
	         					<label class="pos-rel">
	         						<input type="checkbox" class="ace" />
	         						<span class="lbl"></span>
	         					</label>
	         				</td>
	         				<td>
	         					{{ $model->title }}
	         				</td>
	         				<td>
	         					{{ $model->type }}
	         				</td>
	         				<td>
	         					{{ $model->code }}
	         				</td>
	         				<td>
	         					{{ $model->published_at }}
	         				</td>
	         				
	         				<td>
	         					{{ $model->created_at }}
	         				</td>
	         				<td>
	         					{{ $model->updated_at }}
	         				</td>
	         				<td>
	         					<div class="hidden-sm hidden-xs action-buttons">
	         						<a href="{{ URL::to('admin/page/edit/' . $model->id) }}" class="green">
	         							<i class="ace-icon fa fa-pencil bigger-130"></i>
	         						</a>
	         						<a href="{{ URL::to('admin/page/delete/' . $model->id) }}" class="red">
	         							<i class="ace-icon fa fa-trash-o bigger-130"></i>
	         						</a>
	         					</div>
	         				</td>
	         			</tr>
	         			@endforeach
	         		</tbody>
	         	</table>
        	<div>
        		<ul class="ace-thumbnails clearfix">
        		<!--
        			{{-- 
        			@foreach ($clubs as $club)
        				<li>
        					<a href="{{ URL::to('admin/main/edit/' . $club->id) }}" data-rel="colorbox">
        						<span style="display: block; width: 180px; height: 255px; background: url({{ (!empty($image = $club->images->first())) ? $image->source : url('img/noimage.png') }}) 50% 0% no-repeat; background-size: cover;"> </span>
                        	</a>
                        	<div class="tags">
	                            <span class="label-holder">
	                                <span class="label label-info">{{ $club->title }}</span>
	                            </span>
                        	</div>
                        	<div class="tools tools-top in">
                            	<a href="{{ URL::to('admin/main/edit/' . $club->id) }}">
                                	<i class="ace-icon fa fa-pencil"></i>
                            	</a>
                            	<a href="{{ URL::to('admin/main/delete/' . $club->id) }}" class="delete-object">
                                	<i class="ace-icon fa fa-trash-o"></i>
                            	</a>
                        	</div>
        				</li>
        			@endforeach
        			--}}
        		 -->
        		</ul>
        	</div>
		</div>
	</div>
</div>
@endsection