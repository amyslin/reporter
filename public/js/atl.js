!function(factory) {
    "function" == typeof define && define.amd ? define([ "jquery" ], factory) : factory("object" == typeof exports ? require("jquery") : jQuery);
}(function($) {
    $(document).ready(function(){
        $('.daterange').daterangepicker();
        $('.date-picker').datepicker();
        //$('.time-picker').timepicker({
        //    minuteStep: 1,
        //    showMeridian: false
        //}).on('focus', function() {
        //    $(this).timepicker('showWidget');
        //});

        $('#id-input-file-1').ace_file_input({
            no_file:'Выберите файл ...',
            btn_choose:'Выбрать',
            btn_change:'Изменить',
            droppable:true,
            onchange:null,
            thumbnail:'large', //| true | large
            whitelist:'png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
        });

        //$('.autotooltip')
        //    .click(function(e){
        //        e.preventDefault();
        //    })
        //    .tooltip({
        //        show: null,
        //        position: {
        //            my: "left top",
        //            at: "left bottom"
        //        },
        //        open: function( event, ui ) {
        //            ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
        //        }
        //    });

        $('.chosen-select').chosen();

        $('[name=calendar-type]').change(function(){
            if('weekday' == $('[name=calendar-type]:checked').val()){
                $('#calendar-date').hide();
                $('#calendar-weekday').show();
            }
            else {
                $('#calendar-weekday').hide();
                $('#calendar-date').show();
            }
        });


        //$('#chart').css({'width':'100%' , 'height':'250px'});
        //$.plot('#chart', [
        //    { label: '1т', data: [[0, 10000], [1, 10000], [2, 10000], [3, 10000]], xaxis: 0 },
        //    { label: '2т', data: [[0, 20000], [1, 20000], [2, 20000], [3, 20000]], xaxis: 10 },
        //    { label: '5т', data: [[0, 30000], [1, 30000], [2, 30000], [3, 30000]], xaxis: 20 },
        //    { label: '10т', data: [[0, 25000], [1, 25000], [2, 25000], [3, 25000]], xaxis: 30 }
        //], {
        //    series: {
        //        bars: {
        //            show: true,
        //            barWidth: 0.5,
        //            align: "center"
        //        }
        //    },
        //    xaxis: {
        //        mode: 'categories',
        //        categories: {
        //            '1-10 км': 0,
        //            '11-20 км': 1,
        //            '21-30 км': 2,
        //            '31-40 км': 3
        //        },
        //        tickLength: 0
        //    },
        //    grid: {
        //        backgroundColor: { colors: [ '#fff', '#fff' ] },
        //        borderWidth: 1,
        //        borderColor:'#555'
        //    }
        //});

        Chart.defaults.groupableBar = Chart.helpers.clone(Chart.defaults.bar);

        var helpers = Chart.helpers;
        Chart.controllers.groupableBar = Chart.controllers.bar.extend({
            calculateBarX: function (index, datasetIndex) {
                // position the bars based on the stack index
                var stackIndex = this.getMeta().stackIndex;
                return Chart.controllers.bar.prototype.calculateBarX.apply(this, [index, stackIndex]);
            },

            hideOtherStacks: function (datasetIndex) {
                var meta = this.getMeta();
                var stackIndex = meta.stackIndex;

                this.hiddens = [];
                for (var i = 0; i < datasetIndex; i++) {
                    var dsMeta = this.chart.getDatasetMeta(i);
                    if (dsMeta.stackIndex !== stackIndex) {
                        this.hiddens.push(dsMeta.hidden);
                        dsMeta.hidden = true;
                    }
                }
            },

            unhideOtherStacks: function (datasetIndex) {
                var meta = this.getMeta();
                var stackIndex = meta.stackIndex;

                for (var i = 0; i < datasetIndex; i++) {
                    var dsMeta = this.chart.getDatasetMeta(i);
                    if (dsMeta.stackIndex !== stackIndex) {
                        dsMeta.hidden = this.hiddens.unshift();
                    }
                }
            },

            calculateBarY: function (index, datasetIndex) {
                this.hideOtherStacks(datasetIndex);
                var barY = Chart.controllers.bar.prototype.calculateBarY.apply(this, [index, datasetIndex]);
                this.unhideOtherStacks(datasetIndex);
                return barY;
            },

            calculateBarBase: function (datasetIndex, index) {
                this.hideOtherStacks(datasetIndex);
                var barBase = Chart.controllers.bar.prototype.calculateBarBase.apply(this, [datasetIndex, index]);
                this.unhideOtherStacks(datasetIndex);
                return barBase;
            },

            getBarCount: function () {
                var stacks = [];

                // put the stack index in the dataset meta
                Chart.helpers.each(this.chart.data.datasets, function (dataset, datasetIndex) {
                    var meta = this.chart.getDatasetMeta(datasetIndex);
                    if (meta.bar && this.chart.isDatasetVisible(datasetIndex)) {
                        var stackIndex = stacks.indexOf(dataset.stack);
                        if (stackIndex === -1) {
                            stackIndex = stacks.length;
                            stacks.push(dataset.stack);
                        }
                        meta.stackIndex = stackIndex;
                    }
                }, this);

                this.getMeta().stacks = stacks;
                return stacks.length;
            },
        });

        var data = {
            labels: ["1-10км", "11-20км", "21-30км", "31-40км"],
            datasets: [
                {
                    label: "1т",
                    backgroundColor: "rgba(99,255,132,0.2)",
                    data: [10000, 10000, 10000, 10000],
                    stack: 1
                },
                {
                    label: "2т",
                    backgroundColor: "rgba(99,132,255,0.2)",
                    data: [20000, 20000, 20000, 20000],
                    stack: 2
                },
                {
                    label: "5т",
                    backgroundColor: "rgba(255,99,132,0.2)",
                    data: [30000, 30000, 30000, 30000],
                    stack: 3
                },
                {
                    label: "10т",
                    backgroundColor: "rgba(255,255,0,0.2)",
                    data: [25000, 25000, 25000, 25000],
                    stack: 4
                }
            ]
        };

        var ctx = document.getElementById("chart").getContext("2d");
        new Chart(ctx, {
            type: 'groupableBar',
            data: data,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            max: 40000,
                        },
                        stacked: true,
                    }]
                }
            }
        });

    });
});

