!function(factory) {
    "function" == typeof define && define.amd ? define([ "jquery" ], factory) : factory("object" == typeof exports ? require("jquery") : jQuery);
}(function($) {
    $(document).ready(function(){
    	
    	var fileConfig = {
    			style : 'well',
    			btn_choose : 'Select or drop files here',
    			btn_change: null,
    			droppable: true,
    			thumbnail: 'large',
    			
    			maxSize: null,//bytes
    			allowExt: ["jpeg", "jpg", "png", "gif"],
    			allowMime: ["image/jpg", "image/jpeg", "image/png", "image/gif"],

    			before_remove: function() {
    				if(upload_in_progress)
    					return false;//if we are in the middle of uploading a file, don't allow resetting file input
    				return true;
    			},

    			preview_error: function(filename , code) {
    				//code = 1 means file load error
    				//code = 2 image load error (possibly file is not an image)
    				//code = 3 preview failed
    			}
    		};
        $('.daterange').daterangepicker();
        $('.date-picker').datetimepicker({
        	defaultDate: new Date(),
//        	maskInput: true,
//        	pickDate: true,
//        	pickTime: true,
//        	actualFormat: 'yyyy-MM-dd HH:mm:ss',
//        	pickSeconds: true
        	format: 'YYYY-MM-DD HH:mm:ss'
        });
        $('.time-picker').timepicker({
            minuteStep: 1,
            showMeridian: false
        }).on('focus', function() {
            $(this).timepicker('showWidget');
        });
        
        var $form = $('.form-horizontal');
        
        var mapFill = function() {
        	var address = $form.find('input[name=address]').val();
        	var myGeocoder = ymaps.geocode(address, {results: 1});
        	myGeocoder.then(
        			function (res) {
        				map.geoObjects.each(function(context) {
        			        map.geoObjects.remove(context);
        			    });
        		        map.geoObjects.add(res.geoObjects);
        		        // Выведем в консоль данные, полученные в результате геокодирования объекта.
        				var point = res.geoObjects.properties.get('metaDataProperty').GeocoderResponseMetaData.Point;
        				if (point != undefined) {
        					$form.find('input[name=loc_lat]').val(point.coordinates[1]);
        					$form.find('input[name=loc_lon]').val(point.coordinates[0]);
        					map.setCenter([point.coordinates[1], point.coordinates[0]], 15);
        					map.panTo([point.coordinates[1], point.coordinates[0]]);
        				}
        				
        				
        		        console.log(res.geoObjects.properties.get('metaDataProperty').GeocoderResponseMetaData.Point);
        		    },
        		    function (err) {
        		        // обработка ошибки
        		    }	
        	);
        	return false;
        }
        
        $('.geocode').click(function() {
        	var address = $form.find('input[name=address]').val();
        	var myGeocoder = ymaps.geocode(address, {results: 1});
        	myGeocoder.then(
        			function (res) {
        				map.geoObjects.each(function(context) {
        			        map.geoObjects.remove(context);
        			    });
        		        map.geoObjects.add(res.geoObjects);
        		        // Выведем в консоль данные, полученные в результате геокодирования объекта.
        				var point = res.geoObjects.properties.get('metaDataProperty').GeocoderResponseMetaData.Point;
        				if (point != undefined) {
        					$form.find('input[name=loc_lat]').val(point.coordinates[1]);
        					$form.find('input[name=loc_lon]').val(point.coordinates[0]);
        					map.setCenter([point.coordinates[1], point.coordinates[0]], 15);
        					map.panTo([point.coordinates[1], point.coordinates[0]]);
        				}
        				
        				
        		        console.log(res.geoObjects.properties.get('metaDataProperty').GeocoderResponseMetaData.Point);
        		    },
        		    function (err) {
        		        // обработка ошибки
        		    }	
        	);
        	return false;
        });
        $form.find('input[name=address]').change(function() {
        	var address = $form.find('input[name=address]').val();
        	var myGeocoder = ymaps.geocode(address, {results: 1});
        	myGeocoder.then(
        			function (res) {
        				map.geoObjects.each(function(context) {
        			        map.geoObjects.remove(context);
        			    });
        		        map.geoObjects.add(res.geoObjects);
        		        // Выведем в консоль данные, полученные в результате геокодирования объекта.
        				var point = res.geoObjects.properties.get('metaDataProperty').GeocoderResponseMetaData.Point;
        				if (point != undefined) {
        					$form.find('input[name=loc_lat]').val(point.coordinates[1]);
        					$form.find('input[name=loc_lon]').val(point.coordinates[0]);
        					map.setCenter([point.coordinates[1], point.coordinates[0]], 15);
        					map.panTo([point.coordinates[1], point.coordinates[0]]);
        				}
        				
        				
        		        console.log(res.geoObjects.properties.get('metaDataProperty').GeocoderResponseMetaData.Point);
        		    },
        		    function (err) {
        		        // обработка ошибки
        		    }	
        	);
        	return true;
        });
        
        
        
        $('#add-file-btn').click(function() {
        	var input = $('<input/>').attr({'name':'images[]', 'type':'file'})
        	$('#file-inputs').append(input);
        	input.ace_file_input(fileConfig);
        	return false;
        });
        
        
		//you can have multiple files, or a file input with "multiple" attribute
		var file_input = $form.find('input[type=file]');
		var upload_in_progress = false;
		file_input.ace_file_input(fileConfig);
		file_input.on('file.error.ace', function(ev, info) {
			if(info.error_count['ext'] || info.error_count['mime']) alert('Invalid file type! Please select an image!');
			if(info.error_count['size']) alert('Invalid file size! Maximum 100KB');
			
			//you can reset previous selection on error
			//ev.preventDefault();
			//file_input.ace_file_input('reset_input');
		});
		
		$(".upload-btn").click(function(){
			
			var files = file_input.data('ace_input_files');
			if( !files || files.length == 0 ) return false;
			
			var deferred ;
			
			if( "FormData" in window ) {
				formData_object = new FormData();
				
				$form.find('input[type=file]').each(function(){
					var field_name = $(this).attr('name');
					//for fields with "multiple" file support, field name should be something like `myfile[]`

					var files = $(this).data('ace_input_files');
					if(files && files.length > 0) {
						for(var f = 0; f < files.length; f++) {
							formData_object.append(field_name, files[f]);
						}
					}
				});
				
				upload_in_progress = true;
				file_input.ace_file_input('loading', true);
				
				deferred = $.ajax({
			        url: '/admin/file/upload',
			        type: 'POST',
					processData: false,//important
					contentType: false,//important
					dataType: 'json',
					data: formData_object
//					headers: {
//						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//					}
					/**
					,
					xhr: function() {
						var req = $.ajaxSettings.xhr();
						if (req && req.upload) {
							req.upload.addEventListener('progress', function(e) {
								if(e.lengthComputable) {	
									var done = e.loaded || e.position, total = e.total || e.totalSize;
									var percent = parseInt((done/total)*100) + '%';
									//percentage of uploaded file
								}
							}, false);
						}
						return req;
					},
					beforeSend : function() {
					},
					success : function() {
					}*/
				});
			}
			else {
				deferred = new $.Deferred //create a custom deferred object
				
				var temporary_iframe_id = 'temporary-iframe-'+(new Date()).getTime()+'-'+(parseInt(Math.random()*1000));
				var temp_iframe = 
						$('<iframe id="'+temporary_iframe_id+'" name="'+temporary_iframe_id+'" \
						frameborder="0" width="0" height="0" src="about:blank"\
						style="position:absolute; z-index:-1; visibility: hidden;"></iframe>')
						.insertAfter($form)

				$form.append('<input type="hidden" name="temporary-iframe-id" value="'+temporary_iframe_id+'" />');
				
				temp_iframe.data('deferrer' , deferred);
				//we save the deferred object to the iframe and in our server side response
				//we use "temporary-iframe-id" to access iframe and its deferred object
				
				$form.attr({
							  method: 'POST',
							  enctype: 'multipart/form-data',
							  target: temporary_iframe_id //important
							});

				upload_in_progress = true;
				file_input.ace_file_input('loading', true);//display an overlay with loading icon
				$form.get(0).submit();
				
				
				//if we don't receive a response after 30 seconds, let's declare it as failed!
				ie_timeout = setTimeout(function(){
					ie_timeout = null;
					temp_iframe.attr('src', 'about:blank').remove();
					deferred.reject({'status':'fail', 'message':'Timeout!'});
				} , 30000);
			}
			
			deferred
			.done(function(result) {//success
				//format of `result` is optional and sent by server
				//in this example, it's an array of multiple results for multiple uploaded files
				console.log(result);
				var message = '';
				$('.form-horizontal').find('.file_names').remove();
				for(var i = 0; i < result.files.length; i++) {
					if(result.files[i].id) {
//						var input = $('<input/>').attr({'type' : 'hidden', 'name' : 'files[]', 'value' : result[i].id });
						
						$('.form-horizontal').append('<input type="hidden" class="file_names" name="files[]" value="'+result.files[i].id+'" />');
					}
					
				}
			
			})
			.fail(function(result) {//failure
				alert("There was an error");
			})
			.always(function() {//called on both success and failure
//				if(ie_timeout) clearTimeout(ie_timeout)
				ie_timeout = null;
				upload_in_progress = false;
				file_input.ace_file_input('loading', false);
			});

			deferred.promise();
        });
		$('.remove_photo').click(function() {
			$(this).parent().parent().remove();
			return false;
		});
		
		$('.delete-object').click(function() {
			if (confirm('Вы уверены, что хотите удалить объект'))
				return true;
			else
				return false;
		});
        $('.datetime-picker').datetimepicker({
//format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-arrows ',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        }).next().on(ace.click_event, function(){
            $(this).prev().focus();
        });

        $('.autotooltip')
            .click(function(e){
                e.preventDefault();
            })
            .tooltip({
                show: null,
                position: {
                    my: "left top",
                    at: "left bottom"
                },
                open: function( event, ui ) {
                    ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
                }
            });

        $('.chosen-select').chosen();

        $('[name=calendar-type]').change(function(){
            if('weekday' == $('[name=calendar-type]:checked').val()){
                $('#calendar-date').hide();
                $('#calendar-weekday').show();
            }
            else {
                $('#calendar-weekday').hide();
                $('#calendar-date').show();
            }
        });

        var init = false;

        $('#edit-calendar-tab').on('shown.bs.tab', function(){
            if( init ){
                return;
            }
            init = true;

            if($('#calendar').length){
                var date = new Date(),
                    d = date.getDate(),
                    m = date.getMonth(),
                    y = date.getFullYear();

                var calendar = $('#calendar').fullCalendar({
                    buttonHtml: {
                        prev: '<i class="ace-icon fa fa-chevron-left"></i>',
                        next: '<i class="ace-icon fa fa-chevron-right"></i>'
                    },
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: ''
                    },
                    events: [
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 1),
                            className: 'label-important'
                        },
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 8),
                            className: 'label-important'
                        },
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 15),
                            className: 'label-important'
                        },
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 22),
                            className: 'label-important'
                        },
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 29),
                            className: 'label-important'
                        },
                        {
                            title: 'Rock-n-roll',
                            start: new Date(y, m, 23),
                            className: 'label-success'
                        }
                    ]
                    ,
                    editable: true,
                    droppable: false,
                    selectable: true,
                    selectHelper: true,
                    select: function(start, end, allDay){
                        bootbox.prompt("Редактировать событие:", function(title){
                            if(title !== null){
                                calendar.fullCalendar('renderEvent',
                                    {
                                        title: title,
                                        start: start,
                                        end: end,
                                        allDay: allDay,
                                        className: 'label-info'
                                    },
                                    true // make the event "stick"
                                );
                            }
                        });
                        calendar.fullCalendar('unselect');
                    }
                    ,
                    eventClick: function(calEvent, jsEvent, view){
                        var modalHtml =
    '<div class="modal fade">\
        <div class="modal-dialog">\
            <div class="modal-content">\
                <div class="modal-body">\
                    <button type="button" class="close" data-dismiss="modal" style="margin-top:-10px;">&times;</button>\
                    <form class="no-margin">\
                        <label>Изменить название &nbsp;</label>\
                        <input class="middle" autocomplete="off" type="text" value="' + calEvent.title + '" />\
                        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> Сохранить</button>\
                    </form>\
                </div>\
                <div class="modal-footer">\
                    <button type="button" class="btn btn-sm btn-danger" data-action="delete"><i class="ace-icon fa fa-trash-o"></i> Удалить</button>\
                    <button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Отмена</button>\
                </div>\
            </div>\
        </div>\
    </div>';
                        var modal = $(modalHtml).appendTo('body');
                        modal.find('form').on('submit', function(ev){
                            ev.preventDefault();
                            calEvent.title = $(this).find("input[type=text]").val();
                            calendar.fullCalendar('updateEvent', calEvent);
                            modal.modal("hide");
                        });
                        modal.find('button[data-action=delete]').on('click', function() {
                            calendar.fullCalendar('removeEvents' , function(ev){
                                return (ev._id == calEvent._id);
                            });
                            modal.modal("hide");
                        });
                        modal.modal('show').on('hidden', function(){
                            modal.remove();
                        });
                    }
                });
            }
        });

        if($("#sales-charts").length){
            var d1 = [30, 35, 23, 38, 42, 45], dd1 = [],
                d2 = [13, 15, 14, 19, 21, 20], dd2 = [],
                d3 = [17, 18, 15, 17, 17, 23], dd3 = [],
                dates = ['25.06.2016','26.06.2016','27.06.2016','28.06.2016','29.06.2016','30.06.2016'], daxis = [],
                i, l = dates.length;
            for(i = 0; i < l; i++){
                dd1.push([i, d1[i]]);
                dd2.push([i, d2[i]]);
                dd3.push([i, d3[i]]);
                daxis.push([i, dates[i]]);
            }
            $.plot("#sales-charts", [
                { label: "Просмотры", data: dd1 },
                { label: "Я пойду", data: dd2 },
                { label: "Заявки", data: dd3 }
            ], {
                hoverable: true,
                shadowSize: 0,
                series: {
                    lines: { show: true },
                    points: { show: true }
                },
                xaxis: {
                    tickLength: 0,
                    ticks: daxis
                },
                yaxis: {
                    ticks: 10,
                    min: 10,
                    max: 50,
                    tickDecimals: 0
                },
                grid: {
                    backgroundColor: { colors: [ "#fff", "#fff" ] },
                    borderWidth: 1,
                    borderColor:'#555'
                }
            });
        }
    });
    
    $('#editor').ace_wysiwyg({
		
		'wysiwyg': {
			hotKeys : {} //disable hotkeys
		}
		
	}).prev().addClass('wysiwyg-style1');
    
    $('#form-editor').on('submit', function() {
    	$('#editor-value').val($('#editor').html());
    	return true;
    });
    $('#form-editor').on('reset', function() {
    	$('#editor').empty();
    });
    
    $('.multiselect').multiselect({
   	 enableFiltering: false,
   	 enableHTML: true,
   	 buttonClass: 'btn btn-white btn-primary',
   	 templates: {
   		button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
   		ul: '<ul class="multiselect-container dropdown-menu"></ul>',
   		filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
   		filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
   		li: '<li><a tabindex="0"><label></label></a></li>',
          divider: '<li class="multiselect-item divider"></li>',
          liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
   	 }
   	});
});

