!function(factory) {
    "function" == typeof define && define.amd ? define([ "jquery" ], factory) : factory("object" == typeof exports ? require("jquery") : jQuery);
}(function($) {
    $(document).ready(function(){
        $('.daterange').daterangepicker();
        //$('.date-picker').datepicker();
        //$('.time-picker').timepicker({
        //    minuteStep: 1,
        //    showMeridian: false
        //}).on('focus', function() {
        //    $(this).timepicker('showWidget');
        //});

        $('#id-input-file-1').ace_file_input({
            no_file:'Выберите файл ...',
            btn_choose:'Выбрать',
            btn_change:'Изменить',
            droppable:true,
            onchange:null,
            thumbnail:'large', //| true | large
            whitelist:'png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
        });

        //$('.autotooltip')
        //    .click(function(e){
        //        e.preventDefault();
        //    })
        //    .tooltip({
        //        show: null,
        //        position: {
        //            my: "left top",
        //            at: "left bottom"
        //        },
        //        open: function( event, ui ) {
        //            ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
        //        }
        //    });

        $('.chosen-select').chosen();

        $('[name=calendar-type]').change(function(){
            if('weekday' == $('[name=calendar-type]:checked').val()){
                $('#calendar-date').hide();
                $('#calendar-weekday').show();
            }
            else {
                $('#calendar-weekday').hide();
                $('#calendar-date').show();
            }
        });
    });
});

