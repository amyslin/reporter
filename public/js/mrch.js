!function(factory) {
    "function" == typeof define && define.amd ? define([ "jquery" ], factory) : factory("object" == typeof exports ? require("jquery") : jQuery);
}(function($) {
    $(document).ready(function(){
        $('.chosen-select').chosen();
        $('.daterange').daterangepicker();
        $('.date-picker').datepicker();
        $('.time-picker').timepicker({
            minuteStep: 1,
            showMeridian: false
        }).on('focus', function() {
            $(this).timepicker('showWidget');
        });

        $('.datetime-picker').datetimepicker({
//format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-arrows ',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        }).next().on(ace.click_event, function(){
            $(this).prev().focus();
        });

        $('._moderation')
            //.css('width', '100%')
            .css('height', ($(window).height()-60) + 'px');

        // task remove btns
        var
            checkRemoveBtns = function(){
                var $btns = $('._remove');
                if($btns.length < 2){
                    $btns.hide();
                }
                else {
                    $btns.show();
                }
            },

            initRemoveBtn = function(btn){
                $(btn).click(function(){
                    $(this).closest('.form-group').remove();
                    checkRemoveBtns();
                });
            }
        ;
        checkRemoveBtns();
        initRemoveBtn('._remove');

        $('._clone').click(function(){
            var $this = $(this),
                $base = $this.closest('.form-group'),
                $clone = $base.clone(true);
            $clone.insertAfter($base);
            $clone.find('label').html('');
            checkRemoveBtns();
            initRemoveBtn($clone.find('._remove'));
        });


        // gallery
        var $overflow = '';
        var colorbox_params = {
            rel: 'colorbox',
            reposition:true,
            scalePhotos:true,
            scrolling:false,
            previous:'<i class="ace-icon fa fa-arrow-left"></i>',
            next:'<i class="ace-icon fa fa-arrow-right"></i>',
            close:'&times;',
            current:'{current} of {total}',
            maxWidth:'100%',
            maxHeight:'100%',
            onOpen:function(){
                $overflow = document.body.style.overflow;
                document.body.style.overflow = 'hidden';
            },
            onClosed:function(){
                document.body.style.overflow = $overflow;
            },
            onComplete:function(){
                $.colorbox.resize();
            }
        };
        $('[data-rel="colorbox"]').colorbox(colorbox_params);
        $("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");//let's add a custom loading icon
        $(document).one('ajaxloadstart.page', function(e) {
            $('#colorbox, #cboxOverlay').remove();
        });

    });
});

