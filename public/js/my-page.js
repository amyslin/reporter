!function(factory) {
    "function" == typeof define && define.amd ? define([ "jquery" ], factory) : factory("object" == typeof exports ? require("jquery") : jQuery);
}(function($) {
    $(document).ready(function(){
        console.log('123');
        $('.daterange').daterangepicker();
        $('.date-picker').datepicker();
        $('.time-picker').timepicker({
            minuteStep: 1,
            showMeridian: false
        }).on('focus', function() {
            $(this).timepicker('showWidget');
        });

        $('.autotooltip')
            .click(function(e){
                e.preventDefault();
            })
            .tooltip({
                show: null,
                position: {
                    my: "left top",
                    at: "left bottom"
                },
                open: function( event, ui ) {
                    ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
                }
            });

        $('.chosen-select').chosen();

        $('[name=calendar-type]').change(function(){
            if('weekday' == $('[name=calendar-type]:checked').val()){
                $('#calendar-date').hide();
                $('#calendar-weekday').show();
            }
            else {
                $('#calendar-weekday').hide();
                $('#calendar-date').show();
            }
        });

        var init = false;

        $('#edit-calendar-tab').on('shown.bs.tab', function(){
            if( init ){
                return;
            }
            init = true;

            if($('#calendar').length){
                var date = new Date(),
                    d = date.getDate(),
                    m = date.getMonth(),
                    y = date.getFullYear();

                var calendar = $('#calendar').fullCalendar({
                    buttonHtml: {
                        prev: '<i class="ace-icon fa fa-chevron-left"></i>',
                        next: '<i class="ace-icon fa fa-chevron-right"></i>'
                    },
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: ''
                    },
                    events: [
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 1),
                            className: 'label-important'
                        },
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 8),
                            className: 'label-important'
                        },
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 15),
                            className: 'label-important'
                        },
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 22),
                            className: 'label-important'
                        },
                        {
                            title: 'Ночь танцев',
                            start: new Date(y, m, 29),
                            className: 'label-important'
                        },
                        {
                            title: 'Rock-n-roll',
                            start: new Date(y, m, 23),
                            className: 'label-success'
                        }
                    ]
                    ,
                    editable: true,
                    droppable: false,
                    selectable: true,
                    selectHelper: true,
                    select: function(start, end, allDay){
                        bootbox.prompt("Редактировать событие:", function(title){
                            if(title !== null){
                                calendar.fullCalendar('renderEvent',
                                    {
                                        title: title,
                                        start: start,
                                        end: end,
                                        allDay: allDay,
                                        className: 'label-info'
                                    },
                                    true // make the event "stick"
                                );
                            }
                        });
                        calendar.fullCalendar('unselect');
                    }
                    ,
                    eventClick: function(calEvent, jsEvent, view){
                        var modalHtml =
    '<div class="modal fade">\
        <div class="modal-dialog">\
            <div class="modal-content">\
                <div class="modal-body">\
                    <button type="button" class="close" data-dismiss="modal" style="margin-top:-10px;">&times;</button>\
                    <form class="no-margin">\
                        <label>Изменить название &nbsp;</label>\
                        <input class="middle" autocomplete="off" type="text" value="' + calEvent.title + '" />\
                        <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> Сохранить</button>\
                    </form>\
                </div>\
                <div class="modal-footer">\
                    <button type="button" class="btn btn-sm btn-danger" data-action="delete"><i class="ace-icon fa fa-trash-o"></i> Удалить</button>\
                    <button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Отмена</button>\
                </div>\
            </div>\
        </div>\
    </div>';
                        var modal = $(modalHtml).appendTo('body');
                        modal.find('form').on('submit', function(ev){
                            ev.preventDefault();
                            calEvent.title = $(this).find("input[type=text]").val();
                            calendar.fullCalendar('updateEvent', calEvent);
                            modal.modal("hide");
                        });
                        modal.find('button[data-action=delete]').on('click', function() {
                            calendar.fullCalendar('removeEvents' , function(ev){
                                return (ev._id == calEvent._id);
                            });
                            modal.modal("hide");
                        });
                        modal.modal('show').on('hidden', function(){
                            modal.remove();
                        });
                    }
                });
            }
        });

        if($("#sales-charts").length){
            var d1 = [30, 35, 23, 38, 42, 45], dd1 = [],
                d2 = [13, 15, 14, 19, 21, 20], dd2 = [],
                d3 = [17, 18, 15, 17, 17, 23], dd3 = [],
                dates = ['25.06.2016','26.06.2016','27.06.2016','28.06.2016','29.06.2016','30.06.2016'], daxis = [],
                i, l = dates.length;
            for(i = 0; i < l; i++){
                dd1.push([i, d1[i]]);
                dd2.push([i, d2[i]]);
                dd3.push([i, d3[i]]);
                daxis.push([i, dates[i]]);
            }
            $.plot("#sales-charts", [
                { label: "Просмотры", data: dd1 },
                { label: "Я пойду", data: dd2 },
                { label: "Заявки", data: dd3 }
            ], {
                hoverable: true,
                shadowSize: 0,
                series: {
                    lines: { show: true },
                    points: { show: true }
                },
                xaxis: {
                    tickLength: 0,
                    ticks: daxis
                },
                yaxis: {
                    ticks: 10,
                    min: 10,
                    max: 50,
                    tickDecimals: 0
                },
                grid: {
                    backgroundColor: { colors: [ "#fff", "#fff" ] },
                    borderWidth: 1,
                    borderColor:'#555'
                }
            });
        }
    });
});

