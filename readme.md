# Базовое приложение TopDigital

## Установка

1. Установить копию проекта
	``` sh
		git clone https://amyslin@bitbucket.org/amyslin/basic_app.git [project_dir]
	```
2. Установить зависимости через менеджер пакетов composer
	``` sh
		cd [project_dir]
		composer require
	```
3. Инициализировать приложение, при инициализации создаются базовые роли пользователей
	``` sh
		php artisan bootstrap:init
	```
4. Создать файл настроек .env
	``` sh
		cp .env.example .env
	```
5. Сгенерировать ключ приложения
	``` sh
		php artisan key:generate
	```
6. Создать пользователей с ролью администратора
	``` sh
		php artisan user:create [email]
	```

## Настройка push-уведомлений
Настройка push уведомлений производится в конфиг-файле ``` config/push-notification.php ``` 
``` php
return array(

    'AppIOS'     => array(
        'environment' =>'production',
		// Файл сертификата
    	'certificate' => storage_path('prod_pushcert.pem'),
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),
    'AppAndroid' => array(
        'environment' =>'production',
     	'apiKey'	  =>'AIzaSyCQ_TcbACrm0L_N4ftTN3pbjf2NzdfD8xgAIzaSyDxYvNyUtrEpGauKky-ajXdg9dyt6KJzzY',
        'apiKey'      =>'AAAAYIirRxE:APA91bFXuRFl7kC8nuSdA9jvbj0YCmrEaCxU8GR2MIYffKL_xvCF_Lw7hqJnqhnkvrlrGklKAO0ByuGLj_0DpJi411EboNty5oH6a_JYvclxwsKvImDr-atYYZdDMbdVe-Uh_l783jyO',
        'service'     =>'gcm'
    )

);
```

## Настройка nginx для домена API

Для разделения административной части, фронт-енда и админки по разным доменам (например api.site.ru, admin.site.ru, site.ru) в конфигурационном файле для каждого домена должна быть прописана инструкция с серверным параметром.
При этом каждый конфигурационный файл должен испольщовать в качестве root дериктории исходную папку с проектом. По умолчанию (без использования параметра будет отображен front-end)

Для подключения роутов API необходимо указать инструкцию в конфиге nginx ``` fastcgi_param API_ENV api ```

## Настройка nginx для Админки

Для подключения роутов API необходимо указать инструкцию в конфиге nginx ``` fastcgi_param API_ENV admin ```

# Виджеты

Для облегчения работы с версткой использована система виджетов, аналогичная Yii2 в несколько упрощенном варианте, что позволяет использовать при разработке проекта (админки и фронт-енда) использовать заготовленные заранее элементы интерфейса и исключить копипасты
Вызываются в шаблоне следующим образом

```
@widget('ModelForm', $config, $params...)
```

Для написания собственных виджетов следует наследовать классы пакета для laravel https://github.com/arrilot/laravel-widgets с документацией можно ознакомится по ссылке. В проекте basic.app установка расширения не требуется, все необходимые конфиги и провайдеру уже подключены на уровне приложения, исходные коды установятся при развертывании зависимостей композером.

Для создание нового класса виджета можно использовать консольную команду ``` php artisan make:widget ClassName ```

 

## виджет ModelForm

Виджет для построения формы на основе модели. В основном для разработки интерфейсов админки, но можно так-же использовать для фронт-енда для этого нужно создать отдельную папку для шаблонов с элементами.

Виджет использует шаблоны

```
resources
	\-views
		\-model_form
			widget.blade.php
		
```

Настройки модели для использования в виджете
Созданная модель наследуется от класса App\AppModel

В модели необходимо указать следующие параметры
labels - это значения лейблов для полей в форме, если не указаны используется английская транскрипция, аналогично Yii2

``` php
class Form extends AppModel
{
	protected $labels = [
		'id' => 'id',
		'name' => 'Название',
		'comment' => 'Комментарий',
	];
}
```

### Вызов виджета в шаблоне

Для того, чтобы отобразить виджет в шаблоне необходимо вставить в нужном месте следующую конструкцию

```
@widget('ModelForm', ['fieldsOptions' => [
			'name' => ['type' => 'text'],
			'comment' => ['type' => 'html'],
		],
		'order' => [
			
		],
		'groups' => [
			
		],
		'tabs' => [
			'form' => [
				'name' => 'Форма',
				'fields' => ['name', 'comment'],
				'class' => 'green ace-icon fa fa-pencil-square-o bigger-120',
			],
			'fields' => [
				'name' => 'Поля',
				'content' => 'admin.forms.fields',
				'class' => 'blue ace-icon fa fa-list bigger-120'
			]
		],
		], $model, $errors)
		
```

В параметре fieldOptions передаются настройки полей формы. По умолчанию, если не будет указано поле, будет использовано обычное текстовое поле
возможные вариант: text, html, textarea, file, select, select2, checkbox, datepicker

Если необходимо добавить новый тип поля, то в папке шаблонов необходимо добавить соответствующий файл type.blade.php и уже там сделать верстку его отображения

**order** - порядок сортировки полей, в соответствии с массивом ['field1', 'field2']

**groups** - в разработке (предполагается, что будет возможность использования филд-сетов)

**tabs** - Распределение полей по вкладкам, можно также указать параметр content и указать в нем шаблон, для отображения во вкладке

При указании шаблона, для того чтобы передать в него параметры из контроллера необходимо их предварительно расшарить в контроллере, тогда переменные будут доступны из конечного шаблона

Например

``` php
 View::share('model', $model);
```


## Виджет DynamicForm

Предназначен для отображения динамической формы, составленной в административной части в разделе Forms

Вызов виджета

```
@widget('DynamicForms', $config[
	'resultView' => 'widgets.dynamic_form.result',
	'formOptions' => [
		'enctype' => '',
		'action' => '',
	]
], $formId)
```

**resultView** - Вьюха с сообщением для отображения при успешном сохранении формы
**formOptions** - аттрибуты тэга формы, по умлчанию action должен ссылаться на ту-же страницу где и располагается форма. В роутах нужно прописать на страничку оба метода GET и POST.

**Логика работы виджета** - Если используется метод GET, т.е. форма не заполнена, рендерится форма. Если форма заполнена и валидирована, рендерится вьюха с сообщением для пользователя.


 

# About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](http://patreon.com/taylorotwell):

- **[Vehikl](http://vehikl.com)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Styde](https://styde.net)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).