<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (Auth::check()) {
		return redirect('/admin');
	}
	else
		return redirect('/login');
});

Route::post('login', 'Auth\LoginController@login');
Route::get('login' , 'Auth\LoginController@showLoginForm');
	// Route::get('/'     , 'Auth\LoginController@showLoginForm');
Route::get('logout'     , 'Auth\LoginController@logout');

Route::group(['middleware' => ['auth', 'role:admin']], function() {
	Route::get('/admin', 'Admin\MainController@index');
	Route::get('/admin/main', 'Admin\MainController@index');
	
	Route::get('/admin/users', 'Admin\UsersController@index');
	
	Route::get('/admin/users/create', 'Admin\UsersController@create');
	Route::post('/admin/users/create', 'Admin\UsersController@create');
	
	Route::get('/admin/users/edit/{id}', 'Admin\UsersController@edit');
	Route::post('/admin/users/edit/{id}', 'Admin\UsersController@edit');
	
	Route::get('/admin/users/view/{id}', 'Admin\UsersController@view');
	
	Route::get('/admin/users/delete/{id}', 'Admin\UsersController@delete');
	
	Route::get('/admin/roles', 'Admin\RolesController@index');
	
	Route::get('/admin/roles/create/{type}', 'Admin\RolesController@create');
	Route::post('/admin/roles/create/{type}', 'Admin\RolesController@create');
	
	Route::get('/admin/roles/edit/{type}.{id}', 'Admin\RolesController@edit');
	Route::post('/admin/roles/edit/{type}.{id}', 'Admin\RolesController@edit');
	
	Route::get('/admin/roles/delete/{id}', 'Admin\RolesController@delete');
	Route::post('/admin/roles/delete/{id}', 'Admin\RolesController@delete');
	
	Route::get('/admin/agreement', 'Admin\AgreementController@index');
	Route::post('/admin/agreement', 'Admin\AgreementController@index');
	
	Route::get('/admin/pages', 'Admin\PageController@index');
	
	Route::get('/admin/page/create', 'Admin\PageController@create');
	Route::post('/admin/page/create', 'Admin\PageController@create');
	
	Route::get('/admin/page/edit/{id}', 'Admin\PageController@edit');
	Route::post('/admin/page/edit/{id}', 'Admin\PageController@edit');
	
	Route::get('/admin/page/delete/{id}', 'Admin\PageController@delete');
	
	Route::get('/admin/menu', 'Admin\MenuController@index');
	
	Route::get('/admin/menu/create', 'Admin\MenuController@create');
	Route::post('/admin/menu/create', 'Admin\MenuController@create');
	
	Route::get('/admin/menu/edit/{id}', 'Admin\MenuController@edit');
	Route::post('/admin/menu/edit/{id}', 'Admin\MenuController@edit');
	
	Route::get('/admin/menu/delete/{id}', 'Admin\MenuController@delete');
	
	Route::get('/admin/categories', 'Admin\CategoryController@index');
	
	Route::get('/admin/category/create', 'Admin\CategoryController@create');
	Route::post('/admin/category/create', 'Admin\CategoryController@create');
	
	Route::get('/admin/category/edit/{id}', 'Admin\CategoryController@edit');
	Route::post('/admin/category/edit/{id}', 'Admin\CategoryController@edit');
	
	Route::get('/admin/category/delete/{id}', 'Admin\CategoryController@delete');
	Route::post('/admin/category/delete/{id}', 'Admin\CategoryController@delete');
	
	Route::get('/admin/form/create', 'Admin\FormsController@create');
	Route::post('/admin/form/create', 'Admin\FormsController@create');
	
	Route::get('/admin/form/edit/{id}', 'Admin\FormsController@edit');
	Route::post('/admin/form/edit/{id}', 'Admin\FormsController@edit');
	
	Route::get('/admin/form/view/{id}', 'Admin\FormsController@view');
	//Route::post('/admin/form/view/{id}', 'Admin\FormsController@view');
	
	
	
	Route::get('/admin/forms', 'Admin\FormsController@index');
	
	Route::get('/admin/form', 'Admin\FormController@index');
	Route::post('/admin/form', 'Admin\FormController@index');
	
	Route::get('/admin/dictionaries', 'Admin\DictController@index');
	
	Route::get('/admin/dict/create', 'Admin\DictController@create');
	Route::post('/admin/dict/create', 'Admin\DictController@create');
	
	Route::get('/admin/dict/edit/{id}', 'Admin\DictController@edit');
	Route::post('/admin/dict/edit/{id}', 'Admin\DictController@edit');
	
	Route::get('/admin/phpinfo', function() {
		return phpinfo();
	});
});