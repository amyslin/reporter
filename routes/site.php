<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Menu;

Route::get('/', function () {
	
	$items = Menu::orderBy('order', 'asc')->get();
	
	return view('site.index', ['menu' => $items]);
});

Route::get('/{name}', 'Site\PageController@view');

Route::post('login', 'Auth\LoginController@login');
Route::get('login' , 'Auth\LoginController@showLoginForm');
	// Route::get('/'     , 'Auth\LoginController@showLoginForm');
Route::get('logout'     , 'Auth\LoginController@logout');