<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function() {
	return response()->json(['success' => 'true', 'version' => '1.0']);
});

Route::post('/auth/{service}', 'AuthController@index');



Route::group(['middleware' => 'auth:api'], function () {
	
	
	
	Route::get('/user', 'UserController@index');
	
	Route::get('/user/info', 'UserController@info');
	
	Route::get('/user/settings', 'UserController@settings');
	
	Route::post('/user/settings', 'UserController@settings');
	
	Route::get('/user/email', 'UserController@email');
	
	Route::post('/user/email', 'UserController@email');
	
	Route::get('/user/test', 'UserController@test');

});
	