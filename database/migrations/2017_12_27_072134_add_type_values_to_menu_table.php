<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeValuesToMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	
    	DB::statement("ALTER TABLE app_menu MODIFY COLUMN type ENUM('page', 'view', 'controller', 'link')");
        Schema::table('menu', function (Blueprint $table) {
            //
            $table->integer('order')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	DB::statement("ALTER TABLE app_menu MODIFY COLUMN type ENUM('page', 'view', 'controller')");
        Schema::table('menu', function (Blueprint $table) {
            //
            $table->dropColumn('order');
        });
    }
}
