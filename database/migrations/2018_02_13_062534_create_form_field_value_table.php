<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormFieldValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_field_value', function (Blueprint $table) {
            //
            $table->bigInteger('id', true, true);
            $table->integer('field_id', false, true)->nullable();
            $table->text('text_value')->nullable();
            $table->boolean('bool_value')->nullable();
            $table->decimal('num_value', 20, 10)->nullable();
            $table->integer('int_value')->nullable();
            $table->string('var_value', 255)->nullable();
            $table->dateTime('dt_value')->nullable();
            $table->integer('item_id', false, true)->nullable();
            $table->timestamps();
            
            //$table->primary('id');
            $table->foreign('field_id')->references('id')->on('form_fields')->onDelete('set null');
            $table->foreign('item_id')->references('id')->on('form_item')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('form_field_value');
    }
}
