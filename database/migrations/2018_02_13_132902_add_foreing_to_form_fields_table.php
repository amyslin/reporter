<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeingToFormFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	Schema::table('form_fields', function (Blueprint $table) {
    		$table->foreign('dict_id')->references('id')->on('form_dict')->onDelete('set null');
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    	Schema::table('form_fields', function (Blueprint $table) {
    		$table->dropForeign('dict_id');
    	});
    }
}
