<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_device', function (Blueprint $table) {
        	$table->bigInteger('id', true, true);
        	$table->bigInteger('user_id', false, true)->nullable();
        	$table->string('push_token', 512)->nullable();
        	$table->text('push_settings')->nullable();
            $table->timestamps();
            $table->enum('os_type', ['ios', 'android', 'windows']);
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_device');
    }
}
