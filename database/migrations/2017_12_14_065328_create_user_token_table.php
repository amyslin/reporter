<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tokens', function (Blueprint $table) {
        	$table->string('id', 191);
            $table->string('name', 256)->nullable();
            $table->bigInteger('user_id', false, true);
            $table->enum('service', ['vk', 'facebook', 'app', 'instagram', 'google'])->nullable();
            $table->boolean('revoked')->nullable();
            $table->timestamps();
            $table->timestamp('expires_in')->nullable();
            
            $table->primary('id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tokens');
    }
}
