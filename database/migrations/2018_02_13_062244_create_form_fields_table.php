<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_fields', function (Blueprint $table) {
            //
            $table->integer('id', true, true);
            $table->string('name', 50);
            $table->text('comment')->nullable();
            $table->enum('data_type', ['text', 'datetime', 'int', 'numeric', 'checkbox', 'html', 'textarea'])->nullable();
            $table->boolean('multiple')->nullable();
            $table->integer('form_id', false, true)->nullable();
            $table->integer('dict_id', false, true)->nullable();
            $table->integer('order')->nullable();
            $table->timestamps();
            
            //$table->primary('id');
            $table->foreign('form_id')->references('id')->on('articles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('form_fields');
    }
}
