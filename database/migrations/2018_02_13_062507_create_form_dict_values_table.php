<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormDictValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_dict_values', function (Blueprint $table) {
            //
            $table->bigInteger('id', true, true);
            $table->string('title', 255)->nullable();
            $table->string('value', 255)->nullable();
            $table->integer('dict_id', false, true)->nullable();
            $table->timestamps();
            
           // $table->primary('id');
            $table->foreign('dict_id')->references('id')->on('form_dict')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('form_dict_values');
    }
}
