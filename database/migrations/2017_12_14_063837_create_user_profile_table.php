<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile', function (Blueprint $table) {
            $table->bigInteger('id', true, true);
            $table->bigInteger('user_id', false, true)->nullable();
            $table->timestamps();
            $table->enum('service', ['vk', 'facebook', 'app', 'instagram', 'google'])->nullable();
            $table->bigInteger('source_id', false, true)->nullable();
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('maiden_name', 100)->nullable();
            $table->date('birth_date')->nullable();
            $table->integer('sex')->nullable();
            $table->text('raw_data')->nullable();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile');
    }
}
