<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('menu', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('page_id')->unsigned()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('link')->nullable();
            $table->string('seo_link')->nullable();
            $table->timestamps();
            
            $table->foreign('page_id')->references('id')->on('articles')->onDelete('SET NULL');
            $table->foreign('parent_id')->references('id')->on('menu')->onDelete('SET NULL');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('menu');
    }
}
