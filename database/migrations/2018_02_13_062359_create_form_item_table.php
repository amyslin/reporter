<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_item', function (Blueprint $table) {
            //
            $table->integer('id', true, true);
            $table->bigInteger('user_id', false, true)->nullable();
            $table->string('ip', 50)->nullable();
            $table->string('errors', 255)->nullable();
            $table->integer('form_id', false, true)->nullable();
            $table->timestamps();
            
            //$table->primary('id');
            $table->foreign('form_id')->references('id')->on('form')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('form_item');
    }
}
