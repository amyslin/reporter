<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            //
            $table->string('preview_text')->nullable();
			$table->integer('preview_img', false, true)->nullable();
			$table->integer('full_img', false, true)->nullable();
			
			$table->foreign('preview_img')->references('id')->on('file')->onDelete('SET NULL');
			$table->foreign('full_img')->references('id')->on('file')->onDelete('SET NULL');
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            //
            $table->dropForeign(['preview_img']);
            $table->dropForeign(['full_img']);
            $table->dropColumn('preview_text');
            $table->dropColumn('preview_img');
            $table->dropColumn('full_img');
        });
    }
}
