<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use function GuzzleHttp\json_encode;
use Carbon\Carbon;

class UserProfile extends Model
{
    //
    
	protected $fillable = [
			'service',
			'user_id',
			'source_id',
			'first_name',
			'last_name',
			'maiden_name',
			'sex',
			'birth_date',
			'raw_data',
	];
	
	protected $table = 'user_profile';
	
	public static function createProfile($user_id, $service = 'icon', $data = [])
	{
		
		$profile = UserProfile::where(['user_id' => $user_id, 'service' => $service])->first();
		if (!empty($profile))
			return $profile;
		if (count($data)) {
			$data['birth_date'] = (isset($data['bdate'])) ? (new Carbon($data['bdate']))->format('Y-m-d')  : null;
			$fields = array_merge(
				array_intersect_key($data, array_flip(
						[
								'source_id',
								'first_name',
								'last_name',
								'maiden_name',
								'sex',
								'birth_date',
						]
				)),
				compact('user_id', 'service')
			);
			$fields['source_id'] = (isset($data['user_id']))?$data['user_id']:null;
			$fields['raw_data'] = json_encode($data);
			return UserProfile::create($fields);
		}
		
		return null;
	}
	
	
}
