<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormDictValue extends Model
{
    //
    
	protected $table = 'form_dict_values';
	
	protected $fillable = ['title', 'value', 'dict_id'];
	
}
