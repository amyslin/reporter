<?php

namespace App;

use App\AppModel;

class Form extends AppModel
{
    //
    
	protected $table = 'form';
	
	protected $fillable = ['name', 'comment'];
	
	protected $labels = [
		'id' => 'id',
		'name' => 'Название',
		'comment' => 'Комментарий',
	];
	
	public function fields() {
		return $this->hasMany('App\FormField', 'form_id', 'id');
	}
	
	public function items() {
		return $this->hasMany('App\FormItem', 'form_id', 'id');
	}

}
