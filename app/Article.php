<?php

namespace App;

use App\AppModel;

class Article extends AppModel {
	
	protected $table = 'articles';
	
	protected $fillable = ['title', 'preview_text', 'content', 'name', 'code', 'type', 'active', 'published_at'];
	
	protected $labels = [
			'title' => 'Заголовок',
			'preview_text' => 'Анонс',
			'content' => 'Наименование',
			'code' => 'Код',
	];
	
	public function files() 
	{
		return $this->belongsToMany('App\File', 'article_files', 'article_id', 'file_id');	
	}
	
	public function previewImg()
	{
		return $this->hasOne('App\File', 'id', 'preview_img');
	}
	
	public function fullImg()
	{
		return $this->hasOne('App\File', 'id', 'full_img');
	}
	
	public function categories()
	{
		return $this->belongsToMany('App\Category', 'article_categories', 'article_id', 'category_id');
	}
}