<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class AppModel extends Model
{
	
	protected $labels = [
			'created_at' => 'Дата создания',
			'updated_at' => 'Дата модификации'
	];
	
	public function __construct($attributes = [])
	
	{
		parent::__construct($attributes);
		
		$columns = $this->getTableColumns();
		
		$columns = array_combine($columns, array_map(function($val) {return ucfirst(str_replace('_', ' ', $val));} , $columns));
		
		$this->labels = array_merge($columns, $this->labels);
	}
	
	public function getTableColumns($exclude = [])
	{
		return array_diff($this->getConnection()->getSchemaBuilder()->getColumnListing($this->table), $exclude);
	}
	
	public function getAttributeLabels($attributes = []) 
	{
		if (!count($attributes)) 
			return array_intersect_key($this->labels, array_flip($attributes));
		return $this->labels;
	}
	
	public function getAttributeLabel($name) 
	{	
		return (isset($this->labels[$name])) ? $this->labels[$name] : ucfirst($name);		
	}
	
	
}