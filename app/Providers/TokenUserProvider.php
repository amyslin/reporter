<?php

namespace App\Providers;

use Illuminate\Support\Str;
use Illuminate\Auth\EloquentUserProvider;

class TokenUserProvider extends EloquentUserProvider
{
	
	/**
	 * Retrieve a user by the given credentials.
	 *
	 * @param  array  $credentials
	 * @return \Illuminate\Contracts\Auth\Authenticatable|null
	 */
	public function retrieveByCredentials(array $credentials)
	{
		if (empty($credentials)) {
			return;
		}
		
		// First we will add each credential element to the query as a where clause.
		// Then we can execute the query and, if we found a user, return it in a
		// Eloquent User "model" that will be utilized by the Guard instances.
		$query = $this->createModel()->newQuery();
		
		if (isset($credentials['api_token'])) {
			$query->select(['user.*', 'user_tokens.id as api_token']);
			$query->join('user_tokens', 'user.id', '=', 'user_tokens.user_id');
			$query->where('user_tokens.id', '=', $credentials['api_token']);
			$query->where('user_tokens.revoked', '=', 0);
			unset($credentials['api_token']);
		}
		foreach ($credentials as $key => $value) {
			if (! Str::contains($key, 'password')) {
				$query->where($key, $value);
			}
		}
		
		return $query->first();
	}
	
}
