<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Exceptions\ApiUserProvider;
use Illuminate\Support\Facades\Auth;

class TokenServiceProvider extends ServiceProvider
{
	
	protected $polices = [];
	
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Auth::provider('api', function($app, array $config) {
        	return new ApiUserProvider($app);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
