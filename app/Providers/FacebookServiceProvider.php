<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Facebook\Facebook;
// use Facebook\Fac

class FacebookServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(Facebook::class, function($app) {
        	
        	$client = new \GuzzleHttp\Client;
        	
        	return new Facebook(array_merge(config('fb.config'), ['http_client_handler' => new \App\Guzzle6HttpClient($client)]));
        });
        
    }
}
