<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    	$this->app->singleton('GcmNotification', function ($app) {
    		return new \App\PushNotification();
    	});
    	
    	$this->app->singleton('Menu', function ($app) {
    		return new \App\Http\Widgets\MenuWidget();
    	});
    }
}
