<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
	//
	
	public function throwJson($data)
	{
		return response()->json([
				'error' => true,
				'error_code' => 400,
				'description'=>'JSON not valid',
				'fields' => $data
		], 400);
	}
	
	public function version()
	{
		return response()->json(['success' => 'true', 'version' => '1.0']);
	}
}
