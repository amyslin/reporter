<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FormDict;
use App\FormDictValue;
use Illuminate\Support\Facades\View;
use Validator;

class DictController extends Controller
{
    //
    
	public function index()
	{
		$models = FormDict::all();
		
		return view('admin.dict', ['models' => $models]);
	}
	
	public function create(Request $request)
	{
		$model = new FormDict;
		
		$values = [];
		
		if ($request->getMethod() == 'POST') {
			
			$values = $request->input('fields');
			
			$validator = Validator::make($request->all(), [
					'name' => 'required|min:3',
			]);
			
			if ($validator->fails()) {
				
				$fields = [];
				if (!empty($values))
					foreach($values as $field)
						$fields[] = json_decode($field, true);
						
					View::share('fields' , $fields);
						
					return view('admin.dictionaries.edit', ['model' => $model])->withErrors($validator);
						
			}
			
			$model = FormDict::create([
				'name' => $request->input('name'),
			]);
			
			$model->refresh();
			
			foreach ($values as $value) {
				$data = json_decode($value, true);
				FormDictValue::create([
					'dict_id' => $model->id,
					'title' => $data['title'],
					'value' => $data['value']
				]);
			}
			
			return redirect('admin/dictionaries/');
		}
			
		View::share('fields' , $values);
		
		return view('admin.dictionaries.edit', ['model' => $model, 'errors' => []]);
		
	}
	
	public function edit($id, Request $request)
	{
		$model = FormDict::find($id);
		
		if ($request->getMethod() == 'POST') {
			$values = $request->input('fields');
			
			$validator = Validator::make($request->all(), [
					'name' => 'required|min:3',
			]);
			
			if ($validator->fails()) {
				
				$fields = [];
				if (!empty($values))
					foreach($values as $field)
						$fields[] = json_decode($field, true);
						
				View::share('fields' , $fields);
						
				return view('admin.dictionaries.edit', ['model' => $model])->withErrors($validator);
						
			}
			
			$model->fill($request->all());
			$model->save();
			
			$model->refresh();
			
			foreach ($values as $value) {
				$data = json_decode($value, true);
				FormDictValue::updateOrCreate(
				['id' => isset($data['id']) ? $data['id'] : null],		
				[
						'dict_id' => $model->id,
						'title' => $data['title'],
						'value' => $data['value']
				]);
			}
		}
		
		$values = $model->values()->get()->toArray();
		
		View::share('fields' , $values);
		
		return view('admin.dictionaries.edit', ['model' => $model, 'errors' => []]);
	}
}
