<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\File;
use Illuminate\Support\Facades\View;
use App\Article;
use Validator;
use App\Category;

class PageController extends Controller
{
	public function index() 
	{
		$models = Article::where('id', '<>', 1)->paginate();
		
		return view('admin.pages', ['models' => $models]);
		
	}
	
	public function create(Request $request)
	{
		$categories = Category::all();
		
		if ($request->getMethod() == 'POST') 
		{
			$validator = Validator::make($request->all(), [
					'title' => 'required|min:3',
					'code' => 'required|min:3|unique:articles,code',
					'type' => 'in:page,blog',
					'preview' => 'image',
					'full' => 'image',
			]);
			
			if ($validator->fails())
				return view('admin.page.create', ['categories' => $categories])->withErrors($validator);
			
			$page = Article::create($request->all());
			$page->refresh();
			$page->categories()->sync($request->input('categories', []));
			
			
			
			if ($request->hasFile('preview_img')) {
				$path = $request->preview_img->store('images');
					$preview = File::create([
						'title' => $request->input('title'),
						'mime' => $request->preview_img->getMimeType(),
						'path' => $path,
					]);
					$preview->refresh();
					$page->preview_img = $preview->id;
					$page->save();
				
			}
				
			if ($request->hasFile('full_img')) {
				$path = $request->full_img->store('images');
				
				$full = File::create([
						'title' => $request->input('title'),
						'mime' => $request->full_img->getMimeType(),
						'path' => $path,
				]);
				$full->refresh();
				$page->full_img = $full->id;
				$page->save();
				
				
			}
			
			return redirect('admin/page/edit/' . $page->id);
		}

		return view('admin.page.create', ['categories' => $categories]);
	}
	
	public function edit($id, Request $request)
	{
		$model = Article::find($id);
		$categories = Category::all();
		
		if ($request->getMethod() == 'POST') {
			
			$validator = Validator::make($request->all(), [
					'title' => 'required|min:3',
					'code' => 'required|min:3',
					'type' => 'in:page,blog',
			]);
			
			if ($validator->fails())
				return view('admin.page.edit', ['model' => $model, 'categories' => $categories])->withErrors($validator);
			
			$model->fill($request->all());
			$model->save();
			$model->refresh();
			$model->categories()->sync($request->input('categories', []));
			
			if ($request->hasFile('preview_img')) {
				$path = $request->preview_img->store('images');
				$preview = File::create([
						'title' => $request->input('title'),
						'mime' => $request->preview_img->getMimeType(),
						'path' => $path,
				]);
				$preview->refresh();
				$model->preview_img = $preview->id;
				$model->save();
				
			}
			
			if ($request->hasFile('full_img')) {
				$path = $request->full_img->store('images');
				
				$full = File::create([
						'title' => $request->input('title'),
						'mime' => $request->full_img->getMimeType(),
						'path' => $path,
				]);
				$full->refresh();
				$model->full_img = $full->id;
				$model->save();
				
				
			}
		}
		return view('admin.page.edit', ['model' => $model, 'categories' => $categories]);
	}
	
	public function delete($id)
	{
		$model = Article::find($id);
		$model->delete();
		
		return redirect('/admin/pages');
	}
}