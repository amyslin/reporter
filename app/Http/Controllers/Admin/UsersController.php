<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Validator;
use App\User;
use App\UserToken;
use Ultraware\Roles\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class UsersController extends Controller 
{
	public function index(Request $request)
	{
		// 		$clubs = Club::with('images')
		// 				->orderBy('created_at', 'desc')
		// 				->paginate(30);
		$users = User::with(['profiles'])->orderBy('created_at', 'asc')->paginate(50);
		
		return view('admin.users', ['users' => $users]);
	}
	
	public function edit($id, Request $request)
	{
		//     	$model = Club::where('id', $id)->with('images')->first();
		
		$model = User::where('id', $id)->first();
		
		$groups = Role::all(['id', 'slug']);
		
		$userRoles = $model->roles;
		$roles = $userRoles->keyBy('id');
		
		
		if ($request->getMethod() == 'POST') {
			$validator = Validator::make($request->all(), [
					'password' => 'min:3|confirmed',
					'password_confirmation' => 'required_with:password|min:3',
					'roles' => 'required',
			]);
			
			
			
			
			
			if ($validator->fails())
				return view('admin.users.edit', ['model'=>$model, 'groups' => $groups, 'roles' => $roles->all()])->withErrors($validator);
			
			if (!empty($request->input('password', null))) {
				$model->password = Hash::make($request->input('password', null));
				$model->save();
			}
			$model->syncRoles($request->input('roles', []));
			$model->refresh();
		}
		
		
		return view('admin.users.edit', ['model'=>$model, 'groups' => $groups, 'roles' => $roles->all()]);
	}
	
	public function create(Request $request)
	{
		$groups = Role::all(['id', 'slug']);
		
		if ($request->getMethod() == 'POST') {
			
			
			
			$validator = Validator::make($request->all(), [
					'email' => 'required|email|unique:users,email',
					'password' => 'required|min:3|confirmed',
					'password_confirmation' => 'required|min:3',
					'roles' => 'required',
			]);
			
			if ($validator->fails())
				return view('admin.users.create', ['groups' => $groups])->withErrors($validator);
			
			$user = User::create([
				'username' => $request->input('email', null),
				'email' => $request->input('email', null),
				'password' =>  Hash::make($request->input('password', null)),
			]);
			
			if($user->refresh()) {
				
// 					foreach($request->input('roles', []) as $role) {
// 						$user->attachRoles();
// 					}
					$user->syncRoles($request->input('roles', []));
					$tk = str_random(50);
					$token = new UserToken;
					
					$token->id = $tk;
					$token->user_id = $user->id;
					$token->name = 'test';
					$token->service = 'app';
					$token->expires_in = date("Y-m-d H:i:s", time() + (60 * 60 * 24 * 365));
					
					$token->save();

			}
			
			return redirect('/admin/users/edit/' . $user->id);
		}
		
		
		
		return view('admin.users.create', ['groups' => $groups]);
		
	}
	
	public function view($id, Request $request) 
	{
		$user = User::where('id', $id)->with('profiles')->first();
		
		return view('admin.users.view', ['model' => $user]);
	}
	
	public function delete($id, Request $request)
	{
		$model = User::where('id', $id)->first();
		$model->delete();
		
		return redirect('/admin/users');
	}
}