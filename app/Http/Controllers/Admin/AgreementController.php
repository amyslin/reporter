<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use Validator;

class AgreementController extends Controller
{
    //
    
	public function index(Request $request) {
		$model = Article::find(1);
		if (empty($model)) 
			$model = new Article();
		
		if ($request->getMethod() == 'POST') {
			$validator = Validator::make($request->all(), [
					'title' => 'required|max:255',
			]);
			if($validator->fails()) {
				return view('admin.agreement.edit', ['model' => $model])->withErrors($validator);
			}
			$model->name = 'agreement';
			$model->title = $request->input('title');
			$model->content = $request->input('content');
			$model->save();
			$model->refresh();
		}
		
		return view('admin.agreement.edit', ['model' => $model]);
	}
}
