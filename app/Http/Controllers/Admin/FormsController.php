<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Form;
use App\Article;
use App\File;
use Illuminate\Support\Facades\View;
use Validator;
use App\FormField;
use App\FormDict;
use App\FormItem;

class FormsController extends  Controller
{
	public function index()
	{
		$models = Form::all();
		
		return view('admin.forms', ['models' => $models]);
	}
	
	public function view($id, Request $request) 
	{
		$model = Form::find($id);
		
		return view('admin.forms.view', ['model' => $model]);
		
	}
	
	public function viewItem($id, Request $request)
	{
		
	}
	
	public function create(Request $request)
	{
		
		$model = new Form();
		$fields = [];
		$dictionaries = FormDict::all()->toArray();
		if ($request->getMethod() == 'POST')
		{
			$fields = $request->input('fields');
			
			$validator = Validator::make($request->all(), [
					'name' => 'required|min:3',
			]);
			
			if ($validator->fails()) {
				
				$values = [];
				if (!empty($fields))
				foreach($fields as $field)
					$values[] = json_decode($field, true);
				
				View::share('fields' , $fields);
				
				return view('admin.forms.edit', ['model' => $model])->withErrors($validator);
			
			}
			$model = Form::create([
				'name' => $request->input('name'),
				'comment' => $request->input('comments')
			]);
			
			$model->refresh();
			
			foreach ($fields as $field)
			{
				$values = json_decode($field, true);
				FormField::create([
					'form_id' => $model->id,
					'name' => $values['name'],
					'data_type' => (!empty($values['data_type'])) ? $values['data_type'] : 'text',
					'order' => $values['order'],
					'multiple' => $values['multiple'],
					'comment' => $values['comment'],
					'dict_id' => (!empty($values['dict_id']) && $values['dict_id'] != 'null')?$values['dict_id']:null
				]);
			}
			
			return redirect('admin/forms/');
		}
		
		View::share('fields' , $fields);
		
		View::share('dictionaries', $dictionaries);
		
		return view('admin.forms.edit', ['model' => $model, 'errors' => []]);
		
		
	}
	
	public function edit($id, Request $request)
	{
		$model = Form::find($id);
		
		if ($request->getMethod() == 'POST') {
			$fields = $request->input('fields');
			
			$validator = Validator::make($request->all(), [
					'name' => 'required|min:3',
			]);
			
			if ($validator->fails()) {
				
				$values = [];
				if (!empty($fields))
					foreach($fields as $field)
						$values[] = json_decode($field, true);
						
				View::share('fields' , $fields);
						
				return view('admin.forms.edit', ['model' => $model])->withErrors($validator);
						
			}
			
			$model->fill($request->all());
			$model->save();
			
			foreach ($fields as $field)
			{
				$values = json_decode($field, true);
				FormField::updateOrCreate(
				['id' => $values['id']],		
				[		'form_id' => $model->id,
						'name' => $values['name'],
						'data_type' => (!empty($values['data_type'])) ? $values['data_type'] : 'text',
						'order' => $values['order'],
						'multiple' => $values['multiple'],
						'comment' => $values['comment'],
						'dict_id' => (!empty($values['dict_id']) && $values['dict_id'] != 'null')?$values['dict_id']:null
				]);
			}
		}
		
		$fields = $model->fields()->get()->toArray();
		
		$dictionaries = FormDict::all()->toArray();
		
		View::share('fields' , $fields);
		
		View::share('dictionaries', $dictionaries);
		
		return view('admin.forms.edit', ['model' => $model, 'errors' => []]);
	}
	
}