<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UploadRequest;
use App\Http\Controllers\Controller;
use App\File;

class FileController extends Controller
{
    //
	public function upload(UploadRequest $request) 
    {	
		
    	$files = [];
    	
    	foreach($request->images as $image) {
    		$filename = $image->store('resources');
    		
    		$file = File::create([
    			'title' => $filename,
    			'mime' => $image->getMimeType(),
    			'source' => url('storage/'.$filename)
    		]);
    		$file->refresh();
    		$files[] = $file->toArray();
    		
    	}
    	
    	return response()->json(compact('files'));
    }
}
