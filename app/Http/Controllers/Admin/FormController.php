<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Form;

class FormController extends Controller
{
	public function index() 
	{
		return view('admin.form');
	}
}