<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu;
use App\File;
use Illuminate\Support\Facades\View;
use App\Article;
use Validator;

class MenuController extends Controller
{
	public function index()
	{
		$models = Menu::orderBy('order', 'asc')->get();
		
		return view('admin.menu', ['models' => $models]);
	}
	
	public function create(Request $request)
	{
		$pages = Article::all();
		
		$parents = Menu::all();
		
		if ($request->getMethod() == 'POST') {
			
			$validator = Validator::make($request->all(), [
					'title' => 'required|min:3',
					'type' => 'required|in:page,view,controller,link',
					'page_id' => 'required_if:type,page',
					'view' => 'required_if:type,view',
					'order' => 'numeric',
					'controller' => 'required_if:type,controller',
					'link' => 'required_if:type,link'
			]);
			
			if ($validator->fails())
				return view('admin.menu.create', ['pages' => $pages, 'parents' => $parents, 'parent' => $request->input('parent', null), 'place' => $request->input('place', null)])->withErrors($validator);
			
			$data = $request->all();
			if ($request->has('parent_id')) {
				$parent = Menu::find($request->input('parent_id'));
				$data['place'] = $parent->place;
			}
			$model = Menu::create($data);
			$model->refresh();
			
			return redirect('admin/menu/');
		}
		
		return view('admin.menu.create', ['pages' => $pages, 'parents' => $parents, 'parent' => $request->input('parent', null), 'place' => $request->input('place', null)]);
	}
	
	public function edit($id, Request $request)
	{
		$pages = Article::all();
		
		$model = Menu::find($id);
		
		$parents = Menu::where('id', '<>', $id)->get();
		
		if ($request->getMethod() == 'POST') {
			$validator = Validator::make($request->all(), [
					'title' => 'required|min:3',
					'type' => 'required|in:page,view,controller,link',
					'page_id' => 'required_if:type,page',
					'view' => 'required_if:type,view',
					'order' => 'numeric',
					'controller' => 'required_if:type,controller',
					'link' => 'required_if:type,link'
			]);
			if ($validator->fails())
				return view('admin.menu.edit', ['model' => $model, 'pages' => $pages, 'parents' => $parents])->withErrors($validator);
			
			$data = $request->all();
			if ($request->has('parent_id')) {
				$parent = Menu::find($request->input('parent_id'));
				$data['place'] = $parent->place;
			}
			$model->fill($data);
			$model->save();
			$model->refresh();
		}
		
		return view('admin.menu.edit', ['model' => $model, 'pages' => $pages, 'parents' => $parents]);
	}
	
	public function delete($id)
	{
		$model = Menu::where('id', $id);
		$model->delete();
		
		return redirect('/admin/menu');
	}
}