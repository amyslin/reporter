<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Validator;
use App\User;
use App\UserToken;
use Ultraware\Roles\Models\Role;
use Ultraware\Roles\Models\Permission;

class RolesController extends Controller
{
	public function index()
	{
		$roles = Role::all();
		
		$permissions = Permission::all();
		
		return view('admin.roles', ['roles' => $roles, 'permissions' => $permissions]);
	}
	
	public function create($type, Request $request) 
	{
		$roles = Role::all();

		if ($request->getMethod() == 'POST') {
			$validator = Validator::make($request->all(), [
					'name' => 'required|min:2',
					'slug' => 'required|min:2|unique:roles,slug',
			]);
			
			if ($validator->fails())
				return view('admin.roles.create', ['groups' => $roles])->withErrors($validator);
			
			if ($type == 'role') {
			
				$role = Role::create([
					'name' => $request->input('name', null),
					'slug' => $request->input('slug', null),
					'description' =>$request->input('description', null),
				]);
				
				$role->refresh();
				
				return redirect('/admin/roles/edit/role.' . $role->id);
			}
			
			else if ($type == 'permission') {
				$role = Permission::create([
						'name' => $request->input('name', null),
						'slug' => $request->input('slug', null),
						'description' =>$request->input('description', null),
						'model' => $request->input('model', null)
				]);
				
				$role->refresh();
				
				return redirect('/admin/roles/permission.' . $role->id);
			}
		}
		
		return view('admin.roles.create', ['groups' => $roles, 'type' => $type]);
	}
	
	
	
	public function edit($type = 'role', $id, Request $request)
	{
		
		$model = ($type == "role") ? Role::where('id', $id)->first() :  Permission::where('id', $id)->first();
		
		
		$roles = Role::all();
		
		
		if ($request->getMethod() == 'POST') {
			if ($type == 'role')
				$model->syncPermissions($request->input('permissions', []));
			
			$model->description = $request->input('description', null);
			$model->save();
			$model->refresh();

		}
		
		if ($type == 'role') {
			$perm = $model->permissions;
			$role_perm = $perm->keyBy('id')->all();
		}
		else
			$role_perm = [];
		
		$permissions = ($type == 'role') ? Permission::all() : [];

		return view('admin.roles.edit', [ 'model'=>$model, 'groups' => $roles, 'type' => $type, 'permissions' => $permissions, 'role_permissions' => $role_perm]);
	}
	
	public function delete($type = 'role', $id) 
	{
		$model = ($type == "role") ? Role::where('id', $id)->first() :  Permission::where('id', $id)->first();
		
		$model->delete();
		
		return redirect('/admin/roles');
	}
}