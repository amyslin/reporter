<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\File;
use Illuminate\Support\Facades\View;
use App\Article;
use Validator;
use App\Category;

class CategoryController extends Controller
{
	public function index()
	{
		$models = Category::orderBy('order', 'asc')->paginate();
		
		return view('admin.categories', ['models' => $models]);
		
	}
	
	public function create(Request $request) 
	{
		if ($request->getMethod() == 'POST') {
			$validator = Validator::make($request->all(), [
					'title' => 'required|min:3',
					'order' => 'numeric',
					'path' => 'required|min:3',
			]);
			if ($validator->fails())
				return view('admin.caregory.create')->withErrors($validator);
			
			$model = Category::create($request->all());
			
			return redirect('admin/category/edit/' . $model->id);
		}
		
		return view('admin.category.create');
	}
	
	public function edit($id, Request $request)
	{
		$model = Category::find($id);
		
		if ($request->getMethod() == 'POST') {
			
			$validator = Validator::make($request->all(), [
					'title' => 'required|min:3',
					'order' => 'numeric',
					'path' => 'required|min:3',
			]);
			
			if ($validator->fails())
				return view('admin.caregory.edit', ['model' => $model])->withErrors($validator);
		}
		
		return view('admin.category.edit', ['model' => $model]);
	}
	
	public function delete($id) 
	{
		$model = Cateory::find($id);
		$model->delete();
		
		return redirect('/admin/pages');
	}
}