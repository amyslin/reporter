<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Facebook\Facebook;
use ATehnix\VkClient\Client;
// use 
use function GuzzleHttp\json_decode;
use App\User;

class AuthController extends ApiController
{
    //
    
	public function index(Facebook $fb, Request $request, $service)
	
	{
	
		$data = json_decode($request->getContent(), true);
		$data['service'] = $service;
// 		$data['refresh_token'] = false;
		
		$validator = $this->validator($data);
		
		if(!$validator->fails()) {
			switch ($service) {
				case 'vk':
					$userData = $this->checkVk($data);
					if(!empty($userData)) {
						$user = User::getFromProfile($service, array_merge($userData, $data));
						$token = $user->getToken();
					
						$response = [
							'serivce' => $service,
							'token' => $token->id,
// 							'expires' => $token->expires_in,
// 							'refresh_token' => null,
						];
					}
					else
						throw new \Illuminate\Auth\AuthenticationException('Unauthenticated.');
				break;
				case 'facebook':
					$userData = $this->checkFacebook($fb, $data);
					if(!empty($userData)) {
						$user = User::getFromProfile($service, array_merge($userData, $data));
						$token = $user->getToken();
						$response = [
								'serivce' => $service,
								'token' => $token->id,
						];
					}
					else
						throw new \Illuminate\Auth\AuthenticationException('Unauthenticated.');
				break;
				// Тестовый токен
				case 'icon':
					//TODO: 	
					$user = User::join('user_tokens', 'user.id', '=', 'user_tokens.user_id')
					->select(['user_tokens.service as service', 'user_tokens.id as token'])
							->where('user_tokens.service', '=', 'icon')
							->where('user_tokens.revoked', '=', 0)
							->orderBy('user_tokens.created_at', 'DESC')
							->first();
					if (empty($user))
						throw new \Illuminate\Auth\AuthenticationException('Unauthenticated. Token expired.');
					$response = $user->toArray();
					
				break;
			}
			
			return response()->json($response);
		}
		else 
			return $this->throwJson($validator->errors()->all());
	}
	
	protected function validator(array $data) 
	{
		return Validator::make($data, [
			'service' => 'required|in:vk,facebook,app',
			'push_token' => 'max:1024',
			'os_type' => 'in:ios,android',
			'token' => 'required|max:1024',
// 			'expires' => 'required|date',
			'refresh_token' => 'max:1024',
			'user_id' => 'required_if:service,vk,facebook|max:255',
			'email' => 'email',
		]);
	}
	
	
	protected function checkVk(array $data) 
	{
		$client = new Client();
		$client->setDefaultToken($data['token']);
		
		$response = $client->request('storage.get', [
				'key' => 'icon_uid',
		]);
		
		$icon_uid = uniqid('user');
		
		if(isset($response['response']) && empty($response['response'])) {
			
			$client->request('storage.set', [
				'key' => 'icon_uid',
				'value' => $icon_uid,
			]);
		}
		else if (!empty($response['response']))
			$icon_uid = $response['response'];
		
		$response = $client->request('account.getProfileInfo', ['client_secret' => env('VK_APP_SECRET', '')]);
		
		$user_info = [];
		if (isset($data['user_id'])) {
			$user_info = $client->request('users.get', 
					[
						'client_secret' => env('VK_APP_SECRET', ''),
						'user_ids' => $data['user_id'],
						'fields' => 'photo_200',
						'name_case' => 'Nom',
					]);
		}
		
		if (isset($response['response']) && !empty($response['response'])) {
				
			$response['response']['user_id'] = (!empty($data['user_id'])) ? $data['user_id'] : $icon_uid;
			if (isset($user_info['response'][0]['photo_200']) && !empty($user_info['response'][0]['photo_200']))
				$response['response']['avatar'] = $user_info['response'][0]['photo_200'];
			return $response['response'];
		}
		
		return null;
		
	}
	
	
	// TODO: Когда будут ключи реализовать запрос профиля FB
	protected function checkFacebook(Facebook $client, array $data) 
	{
// 		$client = new Facebook();
		
		$client->setDefaultAccessToken($data['token']);
		
		$response = $client->get("me?fields=first_name,last_name,email,gender,birthday");

		
		if ($response->getHttpStatusCode() != 200)
			return null;
		
		$user = $response->getGraphUser();
		if (empty($user))
			return null;
		
		$raw = $response->getDecodedBody();
			
		$r = $client->get($raw['id']."/picture?redirect=false&type=large");
		$avatar = $r->getDecodedBody();
		
		return [
				'email' => $user->getEmail(),
				'first_name' => $user->getFirstName(),
				'last_name' => $user->getLastName(),
				'sex' => (empty($user->getGender())) ? null : (($user->getGender() == 'male') ? 2 : 1),
				'avatar' => (isset($avatar['data']['url']))?$avatar['data']['url']: null,
				'birth_date' => (empty($user->getBirthday())) ? null : date('Y-m-d', strtotime($user->getBirthday()))
		];
		
	}
}
