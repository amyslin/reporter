<?php
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
use App\Menu;

class PageController extends Controller
{
	
	public function view($name, Request $request)
	{
		$page = Article::where('code', $name)->first();
		
		$menu = Menu::orderBy('order', 'asc')->get();
		
		return view('site.page', ['page' => $page, 'menu' => $menu]);
	}
	
}