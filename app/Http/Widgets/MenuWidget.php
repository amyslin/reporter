<?php
namespace App\Http\Widgets;

use App\Menu;

class MenuWidget
{
	public function items($place)
	{
		$all_items = Menu::where('place', '=', $place)->orderBy('order', 'asc')->get();
		
		$grouped = $all_items->mapWithKeys(function ($item) {
			return [$item['id'] => $item];
		})->toArray();
		
		return $this->getTree($grouped);
				
	}
	
	public function adminTree() 
	{
		$all_items = Menu::orderBy('order', 'asc')->get();
		
		$places = $all_items->groupBy('place');
		$items = $places->toArray();
		foreach($items as $key => &$item) {
			$collection = collect($item);
			
			$grouped = $collection->mapWithKeys(function ($item) {
				return [$item['id'] => $item];
			})->toArray();
			
			$item = $this->getTree($grouped);
		}
		return $items;
		
	}
	
	protected function getTree($grouped)
	{
		$tree = [];
		
		foreach ($grouped as $id => &$item)
		{
			if (!$item['parent_id']) {
				$tree[$id] = &$item;
			}
			else {
				$grouped[$item['parent_id']]['childs'][$id] = &$item;
			}
		}
		return $tree;
	}
}