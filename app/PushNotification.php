<?php
namespace App;

use App\Gcm as GcmAdapter;
use Sly\NotificationPusher\PushManager;


class PushNotification extends \Davibennun\LaravelPushNotification\PushNotification
{
	
	public function app($appName)
	{
		$config = is_array($appName) ? $appName : config('push-notification.'.$appName);
		return new App($config);
	}
	
	
	public function GcmAdapter()
	{
		$instance = (new \ReflectionClass('App\GcmAdapter'));
		return $instance->newInstanceArgs(func_get_args());
	}
}

class App extends \Davibennun\LaravelPushNotification\App 
{
	public function __construct($config) 
	{
		$this->pushManager = new PushManager($config['environment'] == "development" ? PushManager::ENVIRONMENT_DEV : PushManager::ENVIRONMENT_PROD);
		
		if ($config['service'] == 'apns')
			$adapterClassName = 'Sly\\NotificationPusher\\Adapter\\'.ucfirst($config['service']);
		if ($config['service'] == 'gcm')
			$adapterClassName = 'App\\'.ucfirst($config['service']);
		
		$adapterConfig = $config;
		unset($adapterConfig['environment'], $adapterConfig['service']);
		
		$this->adapter = new $adapterClassName($adapterConfig);
	}
}



