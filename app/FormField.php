<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormField extends Model
{
    //
    
	protected $table = 'form_fields';
	
	protected $fillable = ['name', 'form_id', 'data_type', 'order', 'multiple', 'comment', 'dict_id'];
	
	
	public function dictionary()
	{
		return $this->hasMany('App\FormDict', 'id', 'dict_id');
	}
	
}
