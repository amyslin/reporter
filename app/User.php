<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Ultraware\Roles\Traits\HasRoleAndPermission as HasRoleAndPermissionTrait;
use Ultraware\Roles\Contracts\HasRoleAndPermission;

use Illuminate\Support\Facades\Hash;
use App\UserProfile;
use App\UserDevice;
use App\UserToken;

class User extends Authenticatable implements HasRoleAndPermission
{
	use Notifiable, HasRoleAndPermissionTrait;
	
	protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function tokens()
    {
    	return $this->hasMany('App\UserToken', 'user_id', 'id')->orderBy('created_at', 'desc');
    }
    
    public function profiles()
    {
    	return $this->hasMany('App\UserProfile', 'user_id', 'id');
    }
    
    public function userDevices() {
    	return $this->hasMany('App\UserDevice', 'user_id', 'id')->orderBy('created_at', 'desc');
    }
    
    public function token($service = null, $data = [])
    {
    	$query = UserToken::query();
    	if (isset($data['token']) && !empty($data['token']))
    		$query->where('id', '=', $data['token']);
    		$query->where('service', $service);
    		$query->where('user_id', $this->id);
//     		$query->where('revoked', 0);
    		$query->orderBy('created_at', 'DESC');
    		$token = $query->first();
    		
    		if (empty($token))
    			$token = $this->createToken($service, $data);
    			$this->setToken($token);
    			return $token;
    }
    
    
    public function setToken($token)
    {
    	$this->token = $token;
    }
    
    public function getToken()
    {
    	return $this->token;
    }
    
    public function getPushSubscribesAttribute($value)
    {
    	$def = ['event' => false, 'news' => false, 'action' => false];
    	if (!empty($value)) {
    		$values = explode(":", $value);
    		$def = array_merge($def, array_combine($values, array_fill(0, count($values), true)));
    	}
    	return $def;
    }
    
    public function setPushSubscribesAttribute($value)
    {
    	$this->attributes['push_subscribes'] = implode(':', array_keys(array_filter($value)));
    }
    
    public function setAvatarAttribute($value)
    {
    	$this->attributes['avatar'] = (empty($value)) ? url('/resources') . "/def_avatar.png" : $value;
    }
    
    
    /**
     * Создает пользователя по профилю
     *
     * @param string $service
     * @param array $data
     * @return \App\User|boolean
     */
    public static function getFromProfile($service = 'vk', $data = [])
    {
    	if (in_array($service, ['vk', 'facebook', 'app'])) {
    		$user = User::where(['username' => (!empty($data['email']))? $data['email'] : $service. $data['user_id'] . "@topdigital.pro"])
    		->first();
    		
    		if (!empty($user)) {
    			$user->token($service, $data);
    			$user->createProfile($service, $data);
    			if (isset($data['push_token']) && isset($data['os_type']))
    				$user->createUserDevice($data['push_token'], $data['os_type']);
    				return $user;
    				
    		}
    		
    		
    		$user = new User;
    		$user->username = (!empty($data['email']))? $data['email'] : $service . $data['user_id'] . "@topdigital.pro";
    		$user->email = (!empty($data['email']))? $data['email'] : null;
    		$user->password  = Hash::make(str_random(10));
    		$user->avatar = (!empty($data['avatar'])) ? $data['avatar'] : null;
    		$user->push_subscribes = ['event' => true, 'news' => true, 'action' => true];
    		$user->save();
    		$user->refresh();
    		$user->token($service, $data);
    		$user->createProfile($service, $data);
    		if (isset($data['push_token']) && isset($data['os_type']))
    			$user->createUserDevice($data['push_token'], $data['os_type']);
    			return $user;
    			
    	}
    	return false;
    }
    
    
    public function createToken($service = 'app', $data = [])
    {
    	$tk = str_random(50);
    	$token = new UserToken;
    	
    	$token->id = (empty($data['token'])) ? $tk : $data['token'];
    	$token->user_id = $this->id;
    	$token->name = $this->username;
    	$token->service = $service;
    	$token->expires_in = (!empty($data['expires'])) ? date('Y-m-d H:i:s', $data['expires']) : date("Y-m-d H:i:s", time() + (60 * 60 * 24 * 365));
    	$token->save();
    	
    	$token->refresh();
    	
    	return $token;
    }
    
    public function createUserDevice($push_token, $os_type, $push_settings = null)
    {
    	$device = UserDevice::where(['user_id' => $this->id, 'push_token' => $push_token])->first();
    	
    	if (!empty($device))
    		return $device;
    		
    		if (!in_array($os_type, ['ios', 'android']))
    			abort(400, 'Wrong OS type must be (ios|android)');
    			
    			return UserDevice::create([
    					'user_id' => $this->id,
    					'push_token' => $push_token,
    					'push_settings' => $push_settings,
    					'os_type' => $os_type,
    			]);
    			
    }
    
    public function createProfile($service = 'app', $data = [])
    {
    	return UserProfile::createProfile($this->id, $service, $data);
    }
    
}
