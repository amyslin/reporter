<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    //
	
	public $incrementing = false;
	
	protected $table = 'user_tokens';
	
	
}
