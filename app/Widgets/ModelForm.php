<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class ModelForm extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    
    public function __construct(array $config = [])
    {
    	$this->addConfigDefaults([
    			'formOptions' => [
    					'class' => 'form-horizontal',
    					'enctype' => 'multipart/form-data'
    			]
    	]);
    	
    	parent::__construct($config);
    }
    
    
    public function run($model, $errors)
    {
        //

        return view('widgets.model_form.widget', [
            'config' => $this->config,
        	'model' => $model,
        	'errors' => collect($errors),
        ]);
    }
}
