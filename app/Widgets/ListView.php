<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class ListView extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        //

        return view('widgets.list_view', [
            'config' => $this->config,
        ]);
    }
}
