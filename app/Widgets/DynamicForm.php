<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Form;
use App\FormItem;
use App\FormFieldValue;
use Auth;
use Illuminate\Http\Request;

class DynamicForm extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];
    
    public function __construct(array $config = [])
    {
    	$this->addConfigDefaults([
    			'resultView' => 'widgets.dynamic_form.result',
    			'formOptions' => [
    					'class' => 'form-horizontal',
    					'enctype' => 'multipart/form-data'
    			]
    	]);
    	
    	parent::__construct($config);
    }

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run($id, $errors)
    {
        //
        
		$model = Form::find($id);
		
		$request = request();
		
		if ($request->getMethod() == 'POST')
		{
			$this->saveForm($model);
			return view($this->config['resultView']);
		}
		
        return view('widgets.dynamic_form.widget', [
            'config' => $this->config,
        	'id' => $id,
        	'model' => $model,
        	'errors' => collect($errors)
        ]);
        
    }
    
    protected function saveForm($model)
    {
    	
    	
    	$request = request();
    	
    	$user = Auth::user();
    	$params = $request->all();
    	$fields = $model->fields()->get();
    	$item = FormItem::create([
    			'user_id' => (!empty($user)) ? $user->id : null,
    			'ip' => $request->ip(),
    			'form_id' => $model->id,
    	]);
    	$item->refresh();
    	foreach ($fields as $field) {
    		$value = (isset($params['field_' . $field->id])) ? $params['field_' . $field->id] : null;
   
    		switch ($field->data_type) {
    			default:
    			case 'text':
    					$type = 'var_value';
    				break;
    			case 'datetime':
    					$type = 'dt_value';
    				break;
    			case 'int':
    					$type = 'int_value';
    				break;
    			case 'dict':
    					$type = 'dict_id';
    				break;
    			case 'numeric':
    					$type = 'num_value';
    				break;
    			case 'checkbox':
    					$type = 'bool_value';
    				break;
    			case 'textarea':
    			case 'html':
    					$type = 'text_value';
    				break;
    		}
    		
    		$data = [
    				'field_id' => $field->id,
    				'item_id' => $item->id,
    				$type => $value,
    		];
    		FormFieldValue::create($data);
    	}
    }
    
    
    //TODO: Сделать валидацию полей, хотябы базовую
    protected function getValidator($mdoel) 
    {
    	
    }
}
