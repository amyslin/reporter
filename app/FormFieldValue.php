<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFieldValue extends Model
{
    //
    
	protected $table = 'form_field_value';
	
	protected $fillable = ['field_id', 'text_value', 'bool_value', 'num_value', 'int_value', 'var_value', 'dt_value', 'item_id'];
	
	
	
	public function field()
	{
		$this->belongsTo('App\FormField');
	}
	
	public function getValueAttribute() 
	{
		$val = array_filter([$this->text_value, $this->bool_value, $this->num_value, $this->int_value, $this->var_value, $this->dt_value]);
		$res = (count($val)) ? array_shift($val) : '';
		return $res;
	}
}
