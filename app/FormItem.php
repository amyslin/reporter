<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FormItem extends Model
{
    //
    
	protected $table = 'form_item';
	
	protected $fillable = ['user_id', 'ip', 'form_id', 'errors'];
	
	
	public function user()
	{
		return $this->belongsTo('App\User');
	}
	
	public function form()
	{
		return $this->belongsTo('App\Form');
	}
	
	public function values() 
	{
		return $this->hasMany('App\FormFieldValue', 'item_id', 'id');
	}
	
	
}
