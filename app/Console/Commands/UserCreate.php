<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Validator;
use App\User;
use App\UserToken;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Laravel\Passport\Token;
use Ultraware\Roles\Models\Role;

class UserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User create command line';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $arguments = $this->arguments();
        $adminRole = Role::where('slug', '=', 'admin')->first();
        
        if (!empty($arguments['username'])) {
        	$validator = Validator::make($arguments, [
        			'username' => 'email'
        	]);
        	
        	if($validator->fails()) {
        		$arguments['email'] = $this->ask('Enter email');
        	}
        	else {
        		$arguments['email'] = $arguments['username'];
        		
        		$validator = Validator::make($arguments, [
        				'email' => 'email'
        		]);
        		if($validator->fails()) {
        			$this->line('not valid Email!');
        			return false;
        		}
        	}
        	
        	$arguments['password'] = $this->secret('Enter password');
        	$passwordConfirm = $this->secret('Re-enter password');
        	if($arguments['password'] != $passwordConfirm) {
        		$this->line('Wrong password!');
        		return false;
        	}
        	
        	$user = new User;
        	$user->username = $arguments['username'];
        	$user->email = $arguments['email'];
        	$user->password = Hash::make($arguments['password']);
//         	$user->admin = 1;
        	if($user->save()) {
        		$tk = str_random(50);
        		$token = new UserToken;
        		
        		$token->id = $tk;
        		$token->user_id = $user->id;
        		$token->name = 'test';
        		$token->service = 'app';
        		$token->expires_in = date("Y-m-d H:i:s", time() + (60 * 60 * 24 * 365));
        		$user->attachRole($adminRole);
        		if($token->save())
        		$this->line("New user succefuly created");
        		
        		
        	}
        	
        	print_r($arguments);
        	return true;
        }
//         $user = new User;
//         $user->username = $
        $this->error('Fail');
    }
}
