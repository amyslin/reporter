<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Bican\Roles\Models\Role;

class Bootstrap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bootstrap:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Application Init Command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        Role::create([
        	'name' => 'Admin',
        	'slug' => 'admin',
        	'description' => '',
        	'level' => 1
        ]);
        
        Role::create([
        	'name' => 'Api',
        	'slug' => 'api',
        ]);
    }
}
