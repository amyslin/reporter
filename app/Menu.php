<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;


class Menu extends Model {
	
	protected $table = 'menu';
	
	protected $fillable = ['title', 'page_id', 'parent_id', 'link', 'seo_link', 'type', 'controller', 'view', 'place', 'order'];
	
	public function active()
	{
		$route = Request::path();
		
		
		switch ($this->type) {
			case "link":
				return $route == $this->link;
			break;
			case "controller":
				return $route == $this->link;
			break;
			case "view":
				return $route == $this->link;
			break;
			case "page":
				$page = $this->page;
				return $route == $page->code;
			break;
		}
		return false;
	}
	
	public function page()
	{
		return $this->hasOne('App\Article', 'id', 'page_id');
	}
	
	public function url() 
	{
		if ($this->type == 'page')
			return '/' . $this->page->code;
		else
			return $this->link;
	}
}