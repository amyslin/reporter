<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    //
    
	protected $table = 'user_device';
	
	protected $fillable = ['user_id', 'push_token', 'push_settings', 'os_type'];
	
	
}
