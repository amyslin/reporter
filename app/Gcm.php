<?php
namespace App;
use ZendService\Google\Gcm\Message as ServiceMessage;

class Gcm extends \Sly\NotificationPusher\Adapter\Gcm
{
	
	
	public function getServiceMessageFromOrigin(array $tokens, \Sly\NotificationPusher\Model\BaseOptionedModel $message)
	{
		
		$data            = $message->getOptions();
		$data['description'] = $message->getText();
		$serviceMessage = new ServiceMessage();
		$serviceMessage->setRegistrationIds($tokens);
		$serviceMessage->setData($data);
// 		$serviceMessage->setNotification(['message' => $message->getText()]);
		$serviceMessage->setCollapseKey($this->getParameter('collapseKey'));
		$serviceMessage->setRestrictedPackageName($this->getParameter('restrictedPackageName'));
		$serviceMessage->setDelayWhileIdle($this->getParameter('delayWhileIdle', false));
		$serviceMessage->setTimeToLive($this->getParameter('ttl', 600));
		$serviceMessage->setDryRun($this->getParameter('dryRun', false));
		
		return $serviceMessage;
	}
}