<?php

namespace App;

use App\AppModel;

class FormDict extends AppModel
{
    //
    
	protected $table = 'form_dict';
	
	protected $fillable = ['name'];
	
	protected $labels = [
		'name' => 'Наименование',
	];
	
	
	public function values()
	{
		return $this->hasMany('App\FormDictValue', 'dict_id', 'id');
	}
}
